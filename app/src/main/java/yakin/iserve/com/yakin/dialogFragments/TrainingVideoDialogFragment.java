package yakin.iserve.com.yakin.dialogFragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;

import java.util.Calendar;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.custom_views.ScalableVideoView;
import yakin.iserve.com.yakin.models.TrainingVideoModel;

/**
 * Created by AMIRULSUFI on 1/4/2018.
 */

public class TrainingVideoDialogFragment extends DialogFragment implements ScalableVideoView.OnVideoPlayBackListener {

    Context c;
    TrainingVideoModel trainingVideoModel;

    public void passModelValue(Context c, TrainingVideoModel trainingVideoModel) {
        this.c = c;
        this.trainingVideoModel = trainingVideoModel;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.full_screen_video_view, container, false);

        FrameLayout fl_container = (FrameLayout) v.findViewById(R.id.fl_container);

        final ScalableVideoView vv = (ScalableVideoView) v.findViewById(R.id.vv);
        vv.setVideoURI(Uri.parse(trainingVideoModel.getVideo_url()));

        final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        final ImageView ivPlay = (ImageView) v.findViewById(R.id.ivPlay);

        vv.requestFocus();
        vv.setListener(this);
        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                vv.seekTo(100);
                progressBar.setVisibility(View.GONE);
//                ivPlay.setVisibility(View.VISIBLE);

            }
        });
        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
//                ivPlay.setVisibility(View.VISIBLE);

            }
        });

        MediaController vidControl = new MediaController(c);

        //Remove the mediaController from it's parent view.
        ((ViewGroup) vidControl.getParent()).removeView(vidControl);
//Add the mediaController to a FrameLayout within your DialogFragment.
        fl_container.addView(vidControl);

        vidControl.setAnchorView(fl_container);
        vv.setMediaController(vidControl);
        vv.start();


        return v;
    }

    @Override
    public void onVideoPlay() {

    }

    @Override
    public void onVideoPause(int position) {

    }


}
