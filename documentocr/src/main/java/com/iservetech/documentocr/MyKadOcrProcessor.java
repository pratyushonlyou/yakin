package com.iservetech.documentocr;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.util.Freezable;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.util.List;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

/**
 * Created by Ze Khan on 04-Jan-18.
 */

public class MyKadOcrProcessor {
    static TextRecognizer textRecognizer;
    public static Bitmap icNumBitmap, icNameBitmap, icAddBitmap, icGenderBitmap;

    public static MyKad performOcr(Bitmap bitmap) {
        Log.d(TAG, "Performing ocr");
        MyKad myKad = new MyKad();

        Mat image = new Mat();
        Utils.bitmapToMat(bitmap, image);

        if (textRecognizer == null)
            textRecognizer = new TextRecognizer.Builder(DocumentScannerActivity.context).build();

        icNumBitmap = cropBitmap(bitmap, MyKad.icNumRect);
        icNameBitmap = cropBitmap(bitmap, MyKad.nameRect);
        icAddBitmap = cropBitmap(bitmap, MyKad.addressRect);
        icGenderBitmap = cropBitmap(bitmap, MyKad.genderRect);

        myKad.address = detectRect(icAddBitmap);
        myKad.icNum = detectRect(icNumBitmap);
        myKad.name = detectRect(icNameBitmap);
        myKad.gender = detectRect(icGenderBitmap);
        myKad = dataFormatting(myKad);
        return myKad;
    }

    private static Bitmap cropBitmap(Bitmap bitmap, Rect area){
        Bitmap cropped = Bitmap.createBitmap(bitmap, area.x, area.y, area.width, area.height);
        return cropped;
    }

    private static String detectRect(Bitmap croppedBitmap) {
        Frame frame = new Frame.Builder().setBitmap(croppedBitmap).build();

        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);
        String text = "";
        for (int i = 0; i < textBlocks.size(); i++) {
            TextBlock item = textBlocks.valueAt(i);
            List<? extends Text> texts = item.getComponents();
            for (int j = 0; j < texts.size(); j++) {
                Log.d(TAG, texts.get(j).getValue());
            }
            text += item.getValue();
            if (i != textBlocks.size() - 1)
                text += "\n";
        }
        text = text.toUpperCase();
        return text;
    }

    private static MyKad dataFormatting(MyKad myKad) {
        if (myKad.icNum.length() > 0) {
            myKad.icNum = myKad.icNum.replace(" ", "");
            if (myKad.icNum.charAt(6) != '-') {
                myKad.icNum = insertIntoString(myKad.icNum, "-", 6);//myKad.icNum.substring(0, 6) + "-" + myKad.icNum.substring(6, myKad.icNum.length());
            }

            if (myKad.icNum.charAt(9) != '-') {
                myKad.icNum = insertIntoString(myKad.icNum, "-", 9);
            }

            myKad.icNum.replace('O', '0').replace('I', '1').replace('L', '1').replace("S", "5");
        }

        if (myKad.gender.length() > 0) {
            String male = "LELAKI";
            String female = "PEREMPUAN";
            if (male.contains(myKad.gender)) {
                myKad.gender = male;
            }
            if (female.contains(myKad.gender)) {
                myKad.gender = female;
            }
        }

        if (myKad.name.length() > 0) {
            myKad.name = myKad.name.replace("\n", " ").replace("0", "O").replace("1", "I").replace("8", "B").replace("5", "S");
        }

        if (myKad.address.length() > 0) {
            int jalanInd = myKad.address.indexOf("JALAN");
            char prevCharBeforeJalanInd = myKad.address.charAt(jalanInd - 1);
            Log.d(TAG, String.valueOf(prevCharBeforeJalanInd));
            if (prevCharBeforeJalanInd != ' ' && prevCharBeforeJalanInd != '\n' && Character.isDigit(prevCharBeforeJalanInd)) {
                myKad.address = insertIntoString(myKad.address, " ", jalanInd);
            }
            int postCodeStart = ordinalIndexOf(myKad.address, "\n", 2) + 1;
            int postcodeLength = 5;
            String oriPostcode = myKad.address.substring(postCodeStart, postCodeStart + postcodeLength);
            String formattedPostcode = oriPostcode;
            formattedPostcode = formattedPostcode.replace('O', '0').replace('I', '1').replace('L', '1');
            myKad.address.replace(oriPostcode, formattedPostcode);
        }

        return myKad;
    }

    private static String insertIntoString(String oriString, CharSequence charSequence, int index) {
        return oriString.substring(0, index) + charSequence + oriString.substring(index, oriString.length());
    }

    private static int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }
}
