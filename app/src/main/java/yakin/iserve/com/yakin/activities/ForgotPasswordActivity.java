package yakin.iserve.com.yakin.activities;

import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import yakin.iserve.com.yakin.R;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rl_container;
    TextInputLayout til_forget_password_email;
    EditText et_forget_password_email;
    Button btn_forgot_password;
    String str_email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.forgot_password));

        initViews();


    }

    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);

        til_forget_password_email = (TextInputLayout) findViewById(R.id.til_forget_password_email);
        et_forget_password_email = (EditText) findViewById(R.id.et_forget_password_email);
        btn_forgot_password = (Button) findViewById(R.id.btn_forgot_password);

        setListeners();
    }

    private void setListeners() {
        btn_forgot_password.setOnClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_forgot_password:
                if (validate()) {
                    //api call
                    Snackbar.make(rl_container, R.string.forgot_password_success_response_txt, Snackbar.LENGTH_LONG).addCallback(new Snackbar.Callback() {
                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            super.onDismissed(transientBottomBar, event);
                            finish();
                        }
                    }).show();
                }
                break;
        }
    }

    private boolean validate() {
        str_email = et_forget_password_email.getText().toString().trim();
        if (str_email.equals("")) {
            Snackbar.make(rl_container, getResources().getString(R.string.email_empty), Snackbar.LENGTH_LONG).show();
            et_forget_password_email.requestFocus();
            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(str_email).matches()) {
            Snackbar.make(rl_container, getResources().getString(R.string.email_invalid), Snackbar.LENGTH_LONG).show();
            et_forget_password_email.requestFocus();
            return false;
        }
        return true;
    }
}
