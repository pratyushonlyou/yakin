package yakin.iserve.com.yakin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.models.CustomerRegProductListModel;
import yakin.iserve.com.yakin.viewHolders.CustomerRegProductListViewHolder;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class ProductListRecyclerViewAdapter extends RecyclerView.Adapter<CustomerRegProductListViewHolder> {

    Context c;
    ArrayList<CustomerRegProductListModel> customerRegProductListModels;

    public ProductListRecyclerViewAdapter(Context c, ArrayList<CustomerRegProductListModel> customerRegProductListModels) {
        this.c = c;
        this.customerRegProductListModels = customerRegProductListModels;
    }


    @Override
    public CustomerRegProductListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CustomerRegProductListViewHolder(LayoutInflater.from(c).inflate(R.layout.row_product_list, parent, false));
    }

    @Override
    public void onBindViewHolder(CustomerRegProductListViewHolder holder, final int position) {
        holder.tv_product.setText(c.getResources().getString(R.string.currency)+" "+customerRegProductListModels.get(position).getPackage_fee());
        holder.tv_product_desc.setText(customerRegProductListModels.get(position).getPayment_month_validity());
        if(customerRegProductListModels.get(position).isChecked()){
            holder.rbtn.setChecked(true);
        }else{
            holder.rbtn.setChecked(false);
        }
        holder.rl_customer_reg_product_row_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyDataSetChanged();
            }
        });
    }



    @Override
    public int getItemCount() {
        return customerRegProductListModels.size();
    }
}
