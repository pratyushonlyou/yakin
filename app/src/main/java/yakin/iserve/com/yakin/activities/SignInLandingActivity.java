package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.constants.ResponseConstants;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.interfaces.APIParamConstants;
import yakin.iserve.com.yakin.interfaces.APIUrls;

public class SignInLandingActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "SignInLandingActivity";
    RelativeLayout rl_container;
    Button btn_signin;
    TextView tv_signup;
    TextView tv_forgot_password;

    TextInputLayout til_username, til_password;
    EditText et_signin_username, et_signin_password;

    String str_username = "", str_password = "";
    String err_username = "", err_pass = "", err_msg = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_landing);

        initViews();
    }

    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);
        til_username = (TextInputLayout) findViewById(R.id.til_username);
        til_password = (TextInputLayout) findViewById(R.id.til_password);
        et_signin_username = (EditText) findViewById(R.id.et_signin_username);
        et_signin_password = (EditText) findViewById(R.id.et_signin_password);
        btn_signin = (Button) findViewById(R.id.btn_signin);
        tv_signup = (TextView) findViewById(R.id.tv_signup);

        tv_forgot_password = (TextView) findViewById(R.id.tv_forgot_password);


        setListeners();
    }

    private void setListeners() {
        btn_signin.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        tv_forgot_password.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                // api call signin
                if (validate()) {
                    loginAPI();
                }
                break;

            case R.id.tv_signup:

//                startActivity(new Intent(SignInLandingActivity.this, SignupActivity.class));
                break;

            case R.id.tv_forgot_password:
                startActivity(new Intent(SignInLandingActivity.this, ForgotPasswordActivity.class));
                break;
        }
    }

    private boolean validate() {
        str_username = et_signin_username.getText().toString().trim();
        str_password = et_signin_password.getText().toString().trim();
//        if (str_username.equals("")) {
//            Snackbar.make(rl_container, getResources().getString(R.string.username_empty), Snackbar.LENGTH_LONG).show();
//            et_signin_username.requestFocus();
//            return false;
//        }
//        if (str_password.equals("")) {
//            Snackbar.make(rl_container, getResources().getString(R.string.password_empty), Snackbar.LENGTH_LONG).show();
//            et_signin_password.requestFocus();
//            return false;
//        }
        return true;
    }

    private void loginAPI() {

        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(rl_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIParamConstants.username, str_username);
        hashMap.put(APIParamConstants.password, str_password);
        hashMap.put(APIParamConstants.cid, APIParamConstants.cid_value);
        hashMap.put(APIParamConstants.cp, APIParamConstants.cp_value);


        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqPostWithLoader(SignInLandingActivity.this, APIUrls.login_url, TAG, hashMap, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);

                    String access_token = obj.optString(ResponseConstants.access_token);
                    Paper.book().write(PaperDBConstants.access_token, access_token);
                    Paper.book().write(PaperDBConstants.isLoggedIn, true);

                    startActivity(new Intent(SignInLandingActivity.this, DashBoardContainerActivity.class));
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank

                err_username = "";
                err_pass = "";


                String json = null;


                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    err_username = HandleErrorMsgFromVolley.trimMessage(json, "username");
                    err_pass = HandleErrorMsgFromVolley.trimMessage(json, "password");


                    show_errors();


                }

            }

        });

    }

    private void show_errors() {
        til_username.setError(null);
        til_password.setError(null);

        if (err_msg != null && !err_msg.equals("")) {
            Snackbar.make(rl_container, err_msg, Snackbar.LENGTH_LONG).show();
        }

        if (err_username != null && !err_username.equals("")) {
            til_username.setError(err_username);
            et_signin_username.requestFocus();
        }
        if (err_pass != null && !err_pass.equals("")) {
            til_password.setError(err_pass);
            et_signin_username.requestFocus();
        }


    }
}
