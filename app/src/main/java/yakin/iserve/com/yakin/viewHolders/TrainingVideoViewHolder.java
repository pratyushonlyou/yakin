package yakin.iserve.com.yakin.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.custom_views.ScalableVideoView;

/**
 * Created by AMIRULSUFI on 1/4/2018.
 */

public class TrainingVideoViewHolder extends RecyclerView.ViewHolder {

    public ScalableVideoView vv;
    public ProgressBar progressBar;
    public ImageView ivPlay;
    public RelativeLayout rl_total;
    public TextView tv_posted_by, tv_posted_date, tv_desc;


    public TrainingVideoViewHolder(View itemView) {
        super(itemView);

        vv = (ScalableVideoView) itemView.findViewById(R.id.vv);
        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        ivPlay = (ImageView) itemView.findViewById(R.id.ivPlay);
        rl_total = (RelativeLayout) itemView.findViewById(R.id.rl_total);

        tv_posted_by = (TextView) itemView.findViewById(R.id.tv_posted_by);
        tv_posted_date = (TextView) itemView.findViewById(R.id.tv_posted_date);
        tv_desc = (TextView) itemView.findViewById(R.id.tv_desc);

    }
}
