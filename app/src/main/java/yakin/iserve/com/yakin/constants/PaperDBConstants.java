package yakin.iserve.com.yakin.constants;

/**
 * Created by AMIRULSUFI on 12/6/2017.
 */

public interface PaperDBConstants {

    String userId = "usesrId";
    String isLoggedIn = "isLoggedIn";

    String access_token = "access_token";

    String user_model = "user_model";


    String customer_model = "customer_model";

    String customer_reg_last_accessed_page = "customer_reg_last_accessed_page";
    String customer_reg_manual_entry = "customer_reg_manual_entry";
    String customer_reg_device_details_page = "customer_reg_device_details_page";
    String customer_reg_package_selection_page = "customer_reg_package_selection_page";
    String customer_reg_confirm_page = "customer_reg_confirm_page";
    String customer_reg_signature_page = "customer_reg_signature_page";
    String customer_reg_thank_you = "customer_reg_thank_you";


    String existing_customer_model = "existing_customer_model";

}
