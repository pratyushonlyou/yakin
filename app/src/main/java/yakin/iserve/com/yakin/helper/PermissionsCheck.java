package yakin.iserve.com.yakin.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import yakin.iserve.com.yakin.constants.PermissionConstants;


/**
 * Created by AMIRULSUFI on 12/8/2017.
 */

public class PermissionsCheck {


    public static void checkLocationPermission(Context c){
        if (ContextCompat.checkSelfPermission(c,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {


//            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)c,
//                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
//
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions((Activity)c,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        PermissionConstants.MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callBack method gets the
                // result of the request.
//            }

        }

    }


    public static void checkInternetAndLocationPermission(Context c){
        if (ContextCompat.checkSelfPermission(c,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(c,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED
                ) {

//
//            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)c,
//                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
//                String test = "Test";
//
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions((Activity)c,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET},
                        PermissionConstants.MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callBack method gets the
                // result of the request.
//            }

        }

    }


    public static boolean checkCameraPermission(Context c){
        if (ContextCompat.checkSelfPermission(c,
                Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED
                ||
                ContextCompat.checkSelfPermission(c,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ||
                ContextCompat.checkSelfPermission(c,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ){

            return false;
        }


        return true;
    }

    public static void requestCameraPermission(Context c){
        ActivityCompat.requestPermissions((Activity)c,
                new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PermissionConstants.MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
    }




    // phone state permission
    public static boolean checkPhoneStateAccessPermission(Context c){
        if (ContextCompat.checkSelfPermission(c,
                Manifest.permission.READ_PHONE_STATE)!= PackageManager.PERMISSION_GRANTED){

            return false;
        }
        return true;
    }

    public static void requestPhoneStatePermission(Context c){
        ActivityCompat.requestPermissions((Activity)c,
                new String[]{Manifest.permission.READ_PHONE_STATE},
                PermissionConstants.MY_PERMISSIONS_READ_PHONE_STATE);
    }

}
