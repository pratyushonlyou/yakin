package yakin.iserve.com.yakin.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;


public class ScalableVideoView extends VideoView {

    private int mVideoWidth;
    private int mVideoHeight;
    private Context mContext;
    private OnVideoPlayBackListener listener;

    public ScalableVideoView(Context context) {
        super(context);
        mContext = context;
    }

    public ScalableVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public ScalableVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        mVideoWidth = 0;
        mVideoHeight = 0;
    }

    public void setListener(OnVideoPlayBackListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        if (mVideoWidth > 0 && mVideoHeight > 0) {
            // If a custom dimension is specified, force it as the measured dimension
            setMeasuredDimension(mVideoWidth, heightMeasureSpec);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public void changeVideoSize(int width, int height) {
        mVideoWidth = width;
        mVideoHeight = height;

        getHolder().setFixedSize(width, height);

        requestLayout();
        invalidate();
    }

    @Override
    public void pause() {
        super.pause();
        if (listener != null)
            listener.onVideoPause(getCurrentPosition());

    }


    @Override
    public void start() {
        super.start();
        if (listener != null)
            listener.onVideoPlay();

    }

    public interface OnVideoPlayBackListener {
        void onVideoPlay();

        void onVideoPause(int position);
    }
}