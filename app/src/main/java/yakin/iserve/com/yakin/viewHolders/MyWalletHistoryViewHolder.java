package yakin.iserve.com.yakin.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import yakin.iserve.com.yakin.R;

/**
 * Created by AMIRULSUFI on 1/2/2018.
 */

public class MyWalletHistoryViewHolder extends RecyclerView.ViewHolder {

public TextView tv_customer_name,tv_date,tv_amount;

    public MyWalletHistoryViewHolder(View itemView) {
        super(itemView);

        tv_customer_name = (TextView)itemView.findViewById(R.id.tv_customer_name);
        tv_date = (TextView)itemView.findViewById(R.id.tv_date);
        tv_amount = (TextView)itemView.findViewById(R.id.tv_amount);

    }
}
