package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.helper.NetworkCheck;

public class SplashScreenActivity extends AppCompatActivity {


    int TIME_OUT = 3000;
    RelativeLayout rl_splash_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        rl_splash_container = (RelativeLayout)findViewById(R.id.rl_splash_container);

        if(!NetworkCheck.isNetworkConnected(this) || !NetworkCheck.isInternetAvailable()){
            Snackbar.make(rl_splash_container, getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
        }

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                if(Paper.book().read(PaperDBConstants.isLoggedIn,false)){
                    Intent i = new Intent(SplashScreenActivity.this, DashBoardContainerActivity.class);
                    startActivity(i);
                }else {
                    Intent i = new Intent(SplashScreenActivity.this, SignInLandingActivity.class);
                    startActivity(i);
                }

                finish();
            }
        }, TIME_OUT);
    }
}
