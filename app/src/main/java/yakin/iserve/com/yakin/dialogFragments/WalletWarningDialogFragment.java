package yakin.iserve.com.yakin.dialogFragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.custom_views.ScalableVideoView;
import yakin.iserve.com.yakin.models.TrainingVideoModel;

/**
 * Created by AMIRULSUFI on 1/4/2018.
 */

public class WalletWarningDialogFragment extends DialogFragment {

    public static String TAG = "WalletWarningDialogFragment";
    Context c;
    TrainingVideoModel trainingVideoModel;

    public OnTopupClickListener callBack;

    public void passModelValue(Context c, TrainingVideoModel trainingVideoModel) {
        this.c = c;
        this.trainingVideoModel = trainingVideoModel;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.dialog_fragment_warning_wallet, container, false);

        Button btn_wallet_topup = (Button) v.findViewById(R.id.btn_wallet_topup);


        btn_wallet_topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.OnTopupClick();
                dismiss();
            }
        });


        return v;
    }


    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }


    public interface OnTopupClickListener {
        void OnTopupClick();
    }
}
