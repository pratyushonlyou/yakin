package yakin.iserve.com.yakin.models;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class CustomerRegProductListModel {


    private String package_fee_id = "";
    private String package_id = "", payment_month_validity = "";
    private boolean checked = false;
    private String package_fee = "";
    private String tier_description = "";
    private String tier_name= "";


    public String getPackage_id() {
        return package_id;
    }

    public void setPackage_id(String package_id) {
        this.package_id = package_id;
    }

    public String getPayment_month_validity() {
        return payment_month_validity;
    }

    public void setPayment_month_validity(String payment_month_validity) {
        this.payment_month_validity = payment_month_validity;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getPackage_fee() {
        return package_fee;
    }

    public void setPackage_fee(String package_fee) {
        this.package_fee = package_fee;
    }

    public String getTier_description() {
        return tier_description;
    }

    public void setTier_description(String tier_description) {
        this.tier_description = tier_description;
    }

    public String getTier_name() {
        return tier_name;
    }

    public void setTier_name(String tier_name) {
        this.tier_name = tier_name;
    }

    public String getPackage_fee_id() {
        return package_fee_id;
    }

    public void setPackage_fee_id(String package_fee_id) {
        this.package_fee_id = package_fee_id;
    }
}
