package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.adapters.ExistingCustomerDevicesRecyclerViewAdapter;
import yakin.iserve.com.yakin.models.CustomerRegModel;
import yakin.iserve.com.yakin.models.DevicesModel;

public class ExistingCustomerDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rl_container;
    TextView tv_name, tv_address;
    RecyclerView rv_devices;
    Button btn_add;

    CustomerRegModel customerRegModel;

    ArrayList<DevicesModel> devicesModels = new ArrayList<>();
    ExistingCustomerDevicesRecyclerViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_customer_details);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.existing_customer));

        if (getIntent().getExtras().getParcelable("existingCustomerRegModel") != null) {
            customerRegModel = getIntent().getExtras().getParcelable("existingCustomerRegModel");
            devicesModels = customerRegModel.devicesModels;
        }


        initViews();
    }

    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_address = (TextView) findViewById(R.id.tv_address);

        rv_devices = (RecyclerView) findViewById(R.id.rv_devices);

        btn_add = (Button) findViewById(R.id.btn_add);

        setListeners();
        setVals();

        // api call
        //generateDummyData();
        adapter = new ExistingCustomerDevicesRecyclerViewAdapter(ExistingCustomerDetailsActivity.this, devicesModels);
        rv_devices.setLayoutManager(new LinearLayoutManager(ExistingCustomerDetailsActivity.this));
        rv_devices.setAdapter(adapter);
    }

    private void setVals() {
        tv_name.setText(customerRegModel.getName());
        tv_address.setText(customerRegModel.getAddress());
    }


    private void setListeners() {
        btn_add.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:

                Bundle b = new Bundle();
                b.putParcelable("existingCustomerRegModel", customerRegModel);
                Intent i = new Intent(ExistingCustomerDetailsActivity.this, ExistingCustomerAddDeviceActivity.class);
                i.putExtras(b);
                startActivity(i);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
