package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.adapters.CustomerRegProductListRecyclerViewAdapter;
import yakin.iserve.com.yakin.dialogFragments.CustomCalendarDialogFragment;
import yakin.iserve.com.yakin.helper.CheckValues;
import yakin.iserve.com.yakin.models.CustomerRegModel;
import yakin.iserve.com.yakin.models.CustomerRegProductListModel;

import static yakin.iserve.com.yakin.constants.OnActivityResultConstants.IMEI_SCAN_REQUEST_CODE;

public class PolicyRenewalCustomerDeviceDetailsActivity extends AppCompatActivity implements View.OnClickListener, CustomCalendarDialogFragment.OnDateSelect {


//    DeviceCompaniesArrayAdapter companiesArrayAdapter;

    //    DeviceCompanyModelColorsArrayAdapter colorsArrayAdapter;

    //    String date_str = "";
//    TextView tv_purchase_date;
//    ImageView iv_scan_imei;
//    EditText et_imei_no;
//    EditText et_device_company_others, et_device_model_others, et_device_color_others;
//    Spinner sp_device_company, sp_device_model, sp_device_color;
//    DeviceCompanyModelsArrayAdapter modelsArrayAdapter;
//    ArrayList<DeviceCompaniesModel> deviceCompaniesModels = new ArrayList<>();


    RelativeLayout rl_container;
    TextView tv_imei_no;
    RecyclerView rv_products;
    Button btn_nxt;

    CustomerRegModel customerRegModel;


    ArrayList<CustomerRegProductListModel> product_list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy_renewal_customer_device_details);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.device_details_title));


        if (getIntent().getExtras().getParcelable("customerRegModel") != null) {
            customerRegModel = getIntent().getExtras().getParcelable("customerRegModel");
        }


        initViews();

    }


    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.ll_container);

//        sp_device_company = (Spinner) findViewById(R.id.sp_device_company);
//        sp_device_model = (Spinner) findViewById(R.id.sp_device_model);
//        sp_device_color = (Spinner) findViewById(R.id.sp_device_color);
//
//        et_device_company_others = (EditText) findViewById(R.id.et_device_company_others);
//        et_device_model_others = (EditText) findViewById(R.id.et_device_model_others);
//        et_device_color_others = (EditText) findViewById(R.id.et_device_color_others);
//        et_imei_no = (EditText) findViewById(R.id.et_imei_no);
//        iv_scan_imei = (ImageView) findViewById(R.id.iv_scan_imei);

//        tv_purchase_date = (TextView) findViewById(R.id.tv_purchase_date);

        tv_imei_no = (TextView) findViewById(R.id.tv_imei_no);
        rv_products = (RecyclerView) findViewById(R.id.rv_products);


        btn_nxt = (Button) findViewById(R.id.btn_nxt);

//        generateDummyData();
        setListeners();
        setVals();

    }


//    private void setVals() {
//        companiesArrayAdapter = new DeviceCompaniesArrayAdapter(PolicyRenewalCustomerDeviceDetailsActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, deviceCompaniesModels);
//        sp_device_company.setAdapter(companiesArrayAdapter);
//
//        modelsArrayAdapter = new DeviceCompanyModelsArrayAdapter(PolicyRenewalCustomerDeviceDetailsActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, deviceCompaniesModels.get(0).deviceCompanyModelsModels);
//        sp_device_model.setAdapter(modelsArrayAdapter);
//
//        colorsArrayAdapter = new DeviceCompanyModelColorsArrayAdapter(PolicyRenewalCustomerDeviceDetailsActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, deviceCompaniesModels.get(0).deviceCompanyModelsModels.get(0).deviceCompanyModelColorsModels);
//        sp_device_color.setAdapter(colorsArrayAdapter);
//    }

    private void setListeners() {

//        sp_device_company.setOnItemSelectedListener(this);
//        sp_device_model.setOnItemSelectedListener(this);
//        sp_device_color.setOnItemSelectedListener(this);

//        iv_scan_imei.setOnClickListener(this);
//        tv_purchase_date.setOnClickListener(this);
        btn_nxt.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_nxt:
                Bundle b = new Bundle();
                b.putParcelable("customerRegModel",customerRegModel);
                Intent i=new Intent(PolicyRenewalCustomerDeviceDetailsActivity.this, PolicyRenewalConfirmActivity.class);
                i.putExtras(b);
                startActivity(i);

                break;
            case R.id.tv_purchase_date:
                CustomCalendarDialogFragment customCalendarDialogFragment = new CustomCalendarDialogFragment();
                customCalendarDialogFragment.callBack = this;
                customCalendarDialogFragment.show(getFragmentManager(), CustomCalendarDialogFragment.TAG);
                break;

//            case R.id.iv_scan_imei:
////                startActivityForResult(new Intent(CustomerDeviceDetailsActivity.this, IMEIScanActivity.class), IMEI_SCAN_REQUEST_CODE);
//                startActivityForResult(new Intent(PolicyRenewalCustomerDeviceDetailsActivity.this, SimpleScannerActivity.class), IMEI_SCAN_REQUEST_CODE);
//                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onDateSelect(String type, String month_name, String dayOfWeek, int year, int month, int dayOfMonth, Calendar c_selected) {
        String y, m, d;
        if (dayOfMonth <= 9) {
            d = "0" + dayOfMonth;
        } else {
            d = dayOfMonth + "";
        }


//       _purchase_date.setText(d + "-" + month_name + "-" + year);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMEI_SCAN_REQUEST_CODE) {
                String imei_code = data.getExtras().get("Barcode").toString();


                if (CheckValues.checkIMEINo(PolicyRenewalCustomerDeviceDetailsActivity.this, rl_container, imei_code)) {
//                    et_imei_no.setText(imei_code);
                }
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void setVals() {

        generateDummyData();

        rv_products.setLayoutManager(new LinearLayoutManager(PolicyRenewalCustomerDeviceDetailsActivity.this));
        CustomerRegProductListRecyclerViewAdapter adapter = new CustomerRegProductListRecyclerViewAdapter(PolicyRenewalCustomerDeviceDetailsActivity.this, product_list);
        rv_products.setAdapter(adapter);

        if (customerRegModel != null) {
            tv_imei_no.setText(customerRegModel.getImei_no());
        }

    }

    private void generateDummyData() {
//        CustomerRegProductListModel model1 = new CustomerRegProductListModel("7", "RM 15","For 3 months");
//        CustomerRegProductListModel model2 = new CustomerRegProductListModel("7", "RM 17","For 6 months");
//        product_list.add(model1);
//        product_list.add(model2);


    }


}
