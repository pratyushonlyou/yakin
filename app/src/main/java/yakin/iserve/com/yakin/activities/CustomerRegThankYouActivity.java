package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.PaperDBConstants;

public class CustomerRegThankYouActivity extends AppCompatActivity implements View.OnClickListener {


    RelativeLayout rl_thankyou_container;
    Button btn_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_reg_thank_you);

        Paper.book().write(PaperDBConstants.customer_reg_last_accessed_page, PaperDBConstants.customer_reg_thank_you);


        rl_thankyou_container = (RelativeLayout) findViewById(R.id.rl_thankyou_container);
        btn_home = (Button) findViewById(R.id.btn_home);
        btn_home.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_home:
                startActivity(new Intent(CustomerRegThankYouActivity.this, DashBoardContainerActivity.class));
                ActivityCompat.finishAffinity(CustomerRegThankYouActivity.this);


                Paper.book().delete(PaperDBConstants.customer_model);
                Paper.book().delete(PaperDBConstants.customer_reg_last_accessed_page);
                Paper.book().delete(PaperDBConstants.customer_reg_manual_entry);
                Paper.book().delete(PaperDBConstants.customer_reg_device_details_page);
                Paper.book().delete(PaperDBConstants.customer_reg_package_selection_page);
                Paper.book().delete(PaperDBConstants.customer_reg_confirm_page);
                Paper.book().delete(PaperDBConstants.customer_reg_signature_page);
                Paper.book().delete(PaperDBConstants.customer_reg_thank_you);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        // do nothing, prevent to go back to the payment page
        Snackbar.make(rl_thankyou_container, getResources().getString(R.string.cant_go_back), Snackbar.LENGTH_LONG).show();
    }



}
