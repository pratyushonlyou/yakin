package yakin.iserve.com.yakin.helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by AMIRULSUFI on 1/3/2018.
 */

public class  HandleErrorMsgFromVolley {

    public static String trimMessage(String json, String key) {
        String trimmedStringArr = null;
        String trimmedString = "";

        try {
            JSONObject obj = new JSONObject(json);
            trimmedStringArr = obj.optString(key);


            if (trimmedStringArr != null && !trimmedStringArr.equals("")) {


                try {
                    JSONArray arr = new JSONArray(trimmedStringArr);
                    if (arr.length() > 0) {
                        for (int i = 0; i < arr.length(); i++) {
                            if (trimmedString.equals(""))
                                trimmedString = String.valueOf(arr.get(i));
                            else
                                trimmedString = trimmedString + "\n" + arr.get(i);

                        }
                        return trimmedString;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    trimmedString = trimmedStringArr.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }
}
