package yakin.iserve.com.yakin.interfaces;


public interface OnPageChange {
    void setOnPageChangeListenerLeft(int pos);

    void setOnPageChangeListenerRight(int pos);
}
