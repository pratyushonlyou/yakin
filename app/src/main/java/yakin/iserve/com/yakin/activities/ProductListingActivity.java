package yakin.iserve.com.yakin.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.adapters.ProductListRecyclerViewAdapter;
import yakin.iserve.com.yakin.models.CustomerRegProductListModel;

public class ProductListingActivity extends AppCompatActivity {


    RelativeLayout rl_container;
    RecyclerView rv_products;

    ProductListRecyclerViewAdapter adapter;
    ArrayList<CustomerRegProductListModel> product_list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_listing);

        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.product_list_title));

        initViews();
    }

    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);
        rv_products = (RecyclerView) findViewById(R.id.rv_products);

        setVals();
    }

    private void setVals() {

//        CustomerRegProductListModel model7 = new CustomerRegProductListModel("7", "RM 15","For 3 months");
//        CustomerRegProductListModel model8 = new CustomerRegProductListModel("7", "RM 17","For 6 months");
//
//        product_list.add(model7);
//        product_list.add(model8);

        rv_products.setLayoutManager(new LinearLayoutManager(ProductListingActivity.this));
        adapter = new ProductListRecyclerViewAdapter(ProductListingActivity.this, product_list);
        rv_products.setAdapter(adapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

}
