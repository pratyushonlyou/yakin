package yakin.iserve.com.yakin.gatewayNInterfaces;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.WindowManager;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.helper.Logger;
import yakin.iserve.com.yakin.interfaces.APIParamConstants;


public class NetworkRequest {
    private static NetworkRequest ourInstance;
    private static Context mContext;

    public static NetworkRequest getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new NetworkRequest(context);
        }
        return ourInstance;
    }

    private NetworkRequest(Context context) {
        this.mContext = context;
    }

    public void strReqGetWithoutLoader(final String url, final String tag, final MyNetworkResponse responseCallback) {
        StringRequest volleyStrReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                return params;
            }
        };

        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    public void strReqPostWithoutLoader(final String url, final String tag, final Map<String, String> map,
                                       final MyNetworkResponse responseCallback) {
        StringRequest volleyStrReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError.getMessage());
                Logger.printStackTrace(volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                return params;
            }
        };
        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    public void strReqGetWithLoader(Context _context, final String url, final String tag, final MyNetworkResponse responseCallback) {
        final ProgressDialog mProgressDialog = PD(_context);
        StringRequest volleyStrReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError.getMessage());
                Logger.printStackTrace(volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put(APIParamConstants.Authorization, APIParamConstants.Bearer+" "+ Paper.book().read(PaperDBConstants.access_token));
                return params;
            }
        };
        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    public void strReqPostWithLoader(Context _context, final String url, final String tag, final Map<String, String> map,
                                 final MyNetworkResponse responseCallback) {
        final ProgressDialog mProgressDialog = PD(_context);
        StringRequest volleyStrReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showInfoLog("PARAMETERS: " + map);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError.getMessage());
                Logger.printStackTrace(volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put(APIParamConstants.Authorization, APIParamConstants.Bearer+" "+ Paper.book().read(PaperDBConstants.access_token));
                return params;
            }
        };
        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }

    // Loader with without Loading text
    private ProgressDialog PD(Context pdContext) {

        try {
            final ProgressDialog dialog = new ProgressDialog(pdContext);
            dialog.setCanceledOnTouchOutside(false);
            try {
                dialog.show();
                dialog.setContentView(R.layout.progress_layout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            } catch (WindowManager.BadTokenException | IllegalStateException e) {
                Logger.printStackTrace(e);
            }
            return dialog;
        } catch (NullPointerException e) {
            Logger.printStackTrace(e);
            return null;
        }

    }



    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void strReqGetWithLoaderWithAccessTokenHeader(Context _context, final String url, final String tag, final MyNetworkResponse responseCallback) {
        final ProgressDialog mProgressDialog = PD(_context);
        StringRequest volleyStrReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showDebugLog("RESPONSE: " + s);

                if (null != responseCallback) {
                    responseCallback.onSuccessResponse(s);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Logger.setLogTag(tag);
                Logger.showDebugLog("URL: " + url);
                Logger.showErrorLog("ERROR RESPONSE: " + volleyError.getMessage());
                Logger.printStackTrace(volleyError);

                if (null != responseCallback) {
                    responseCallback.onErrorResponse(volleyError);
                } else {
                    Logger.showErrorLog("CALLBACK RESPONSE: " + "MyNetworkResponse callback is null");
                }
                mProgressDialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put(APIParamConstants.Authorization, APIParamConstants.Bearer+" "+ Paper.book().read(PaperDBConstants.access_token));
                return params;
            }
        };
        // Access the RequestQueue through your singleton class.
        VolleyRequestQueue.getInstance(mContext).addToRequestQueue(volleyStrReq, tag);
    }
}
