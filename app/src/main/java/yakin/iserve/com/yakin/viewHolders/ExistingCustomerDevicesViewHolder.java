package yakin.iserve.com.yakin.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import yakin.iserve.com.yakin.R;

/**
 * Created by AMIRULSUFI on 1/9/2018.
 */

public class ExistingCustomerDevicesViewHolder extends RecyclerView.ViewHolder {

    public TextView tv_make, tv_model, tv_purchase_date, tv_package, tv_validity;

    public ExistingCustomerDevicesViewHolder(View itemView) {
        super(itemView);

        tv_make = (TextView)itemView.findViewById(R.id.tv_make);
        tv_model = (TextView)itemView.findViewById(R.id.tv_model);
        tv_purchase_date = (TextView)itemView.findViewById(R.id.tv_purchase_date);
        tv_package = (TextView)itemView.findViewById(R.id.tv_package);
        tv_validity = (TextView)itemView.findViewById(R.id.tv_validity);
    }
}
