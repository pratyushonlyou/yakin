package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Calendar;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.OnActivityResultConstants;
import yakin.iserve.com.yakin.dialogFragments.CustomCalendarDialogFragment;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.models.CustomerRegModel;

public class PolicyRenewalManualEntryActivity extends AppCompatActivity implements View.OnClickListener, CustomCalendarDialogFragment.OnDateSelect {

    ImageView iv_customer_img;

    EditText et_user_fname, et_user_lname, et_user_address, et_user_ic, et_user_dob, et_user_email, et_user_phone_no;
    Button btn_nxt;

    String dob_str = "";
    CustomerRegModel customerRegModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // If we have a saved state then we can restore it now
        if (savedInstanceState != null) {
            img_path = savedInstanceState.getString(STATE_COUNTER);
        }

        if (getIntent().getExtras().getParcelable("customerRegModel") != null) {
            customerRegModel = getIntent().getExtras().getParcelable("customerRegModel");
        }


        setContentView(R.layout.activity_policy_renewal_manual_entry);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.user_info));


        initViews();
        setVals();

    }

    private void setVals() {

        // setting some dummy data ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        customerRegModel.setName("Peter Smith");
        customerRegModel.setAddress("Street 12 KL, MY");
        customerRegModel.setIdentification_number("XXXXXXXXXXXXXXXX");
        customerRegModel.setEmail("peter.smith@gmail.com");
        customerRegModel.setPhone_no("+60 0000000000");


        if (!img_path.equals(null) && !img_path.equals("")) {
            iv_customer_img.setImageURI(null);
            iv_customer_img.setImageURI(Uri.parse(img_path));
        }

        // setting values coming from previous activity
        if (customerRegModel != null) {
            if (!customerRegModel.getName().equals(null) || !customerRegModel.getName().equals("")) {
                et_user_fname.setText(customerRegModel.getName());
            }
            if (!customerRegModel.getAddress().equals(null) || !customerRegModel.getAddress().equals("")) {
                et_user_address.setText(customerRegModel.getAddress());
            }
            if (!customerRegModel.getIdentification_number().equals(null) || !customerRegModel.getIdentification_number().equals("")) {
                et_user_ic.setText(customerRegModel.getIdentification_number());
            }
            if (!customerRegModel.getEmail().equals(null) || !customerRegModel.getEmail().equals("")) {
                et_user_email.setText(customerRegModel.getEmail());
            }
            if (!customerRegModel.getPhone_no().equals(null) || !customerRegModel.getPhone_no().equals("")) {
                et_user_phone_no.setText(customerRegModel.getPhone_no());
            }
        }

    }

    private void initViews() {
        iv_customer_img = (ImageView) findViewById(R.id.iv_customer_img);
        et_user_fname = (EditText) findViewById(R.id.et_user_fname);
        et_user_lname = (EditText) findViewById(R.id.et_user_lname);
        et_user_address = (EditText) findViewById(R.id.et_user_address);
        et_user_ic = (EditText) findViewById(R.id.et_user_ic);
        et_user_dob = (EditText) findViewById(R.id.et_user_dob);
        et_user_email = (EditText) findViewById(R.id.et_user_email);
        et_user_phone_no = (EditText) findViewById(R.id.et_user_phone_no);
        btn_nxt = (Button) findViewById(R.id.btn_nxt);


        setListeners();
    }


    private void setListeners() {
        et_user_fname.setFocusable(false);
        et_user_address.setFocusable(false);
        et_user_ic.setFocusable(false);
        et_user_email.setFocusable(false);
        et_user_phone_no.setFocusable(false);


        iv_customer_img.setOnClickListener(this);

        et_user_dob.setClickable(true);
        et_user_dob.setOnClickListener(this);

        btn_nxt.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_customer_img:
                if (PermissionsCheck.checkCameraPermission(PolicyRenewalManualEntryActivity.this)) {

                    startActivityForResult(new Intent(PolicyRenewalManualEntryActivity.this, CustomerImageSnapActivity.class), OnActivityResultConstants.FACE_DETECTOR_FROM_MANUAL_ENTRY);

                } else {
                    PermissionsCheck.requestCameraPermission(PolicyRenewalManualEntryActivity.this);
                }
                break;

            case R.id.et_user_dob:
                CustomCalendarDialogFragment customCalendarDialogFragment = new CustomCalendarDialogFragment();
                customCalendarDialogFragment.callBack = this;
                customCalendarDialogFragment.show(getFragmentManager(), CustomCalendarDialogFragment.TAG);

                break;
            case R.id.btn_nxt:
                Intent i = new Intent(PolicyRenewalManualEntryActivity.this, PolicyRenewalCustomerDeviceDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("customerRegModel", customerRegModel);
                i.putExtras(bundle);
                startActivity(i);
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == OnActivityResultConstants.FACE_DETECTOR_FROM_MANUAL_ENTRY) {
                Log.e("image", String.valueOf(data));
                img_path = (String) data.getExtras().get("img_path");

                iv_customer_img.setImageURI(null);
                iv_customer_img.setImageURI(Uri.parse(img_path));
            }
        }
    }


    String img_path = "";
    private static final String STATE_COUNTER = "view_state";

    private Bundle state_bundle;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState);
        // Save our own state now
        outState.putString(STATE_COUNTER, img_path);
    }

    @Override
    public void onDateSelect(String type, String month_name, String dayOfWeek, int year, int month, int dayOfMonth, Calendar c_selected) {
        String y, m, d;
        if (dayOfMonth <= 9) {
            d = "0" + dayOfMonth;
        } else {
            d = dayOfMonth + "";
        }


        dob_str = d + "-" + month_name + "-" + year;
        et_user_dob.setText(d + "-" + month_name + "-" + year);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
