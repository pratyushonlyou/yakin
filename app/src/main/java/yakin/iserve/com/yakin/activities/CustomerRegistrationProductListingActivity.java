package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.adapters.CustomerRegProductListRecyclerViewAdapter;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.interfaces.APIParamConstants;
import yakin.iserve.com.yakin.interfaces.APIUrls;
import yakin.iserve.com.yakin.models.CustomerRegModel;
import yakin.iserve.com.yakin.models.CustomerRegProductListModel;

public class CustomerRegistrationProductListingActivity extends AppCompatActivity implements View.OnClickListener {

    public static String TAG = "CustomerRegistrationProductListingActivity";
    RelativeLayout rl_container;
    RecyclerView rv_products;
    Button btn_nxt;

    ArrayList<CustomerRegProductListModel> product_list = new ArrayList<>();
    CustomerRegProductListRecyclerViewAdapter adapter;
    CustomerRegModel customerRegModel = new CustomerRegModel();
    String err_msg = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_registration_product_listing);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.product_list_title));


        customerRegModel = Paper.book().read(PaperDBConstants.customer_model);


        initViews();

    }

    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);
        rv_products = (RecyclerView) findViewById(R.id.rv_products);
        btn_nxt = (Button) findViewById(R.id.btn_nxt);

        setVals();
        setListeners();

        getPackagesAPI();
    }


    private void setVals() {

        rv_products.setLayoutManager(new LinearLayoutManager(CustomerRegistrationProductListingActivity.this));
        adapter = new CustomerRegProductListRecyclerViewAdapter(CustomerRegistrationProductListingActivity.this, product_list);
        rv_products.setAdapter(adapter);

    }


    private void setListeners() {
        btn_nxt.setOnClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_nxt:
                if(validate()) {
                    CustomerRegModel model = Paper.book().read(PaperDBConstants.customer_model);
                    model.setCustomerRegProductListModel(getSelectedPackageModel());
                    Paper.book().write(PaperDBConstants.customer_model, model);
                    //Paper.book().write(PaperDBConstants.customer_reg_last_accessed_page, PaperDBConstants.customer_reg_package_selection_page);

                    startActivity(new Intent(CustomerRegistrationProductListingActivity.this, CustomerRegConfirmActivity.class));
                }
                break;
        }
    }

    private boolean validate() {
        for(int i=0; i<product_list.size();i++){
            if(product_list.get(i).isChecked()){        // atlease one product needs to be selected
                return true;
            }
        }
        Snackbar.make(rl_container, getResources().getString(R.string.err_product_choosen),Snackbar.LENGTH_LONG).show();
        return false;
    }

    private CustomerRegProductListModel getSelectedPackageModel() {
        CustomerRegProductListModel customerRegProductListModel = new CustomerRegProductListModel();
        for (int i = 0; i < product_list.size(); i++) {
            if (product_list.get(i).isChecked()) {
                customerRegProductListModel = product_list.get(i);
                break;
            }
        }
        return customerRegProductListModel;
    }


    private void getPackagesAPI() {
        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(rl_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqGetWithLoader(this, APIUrls.customer_package_fee_url + "?" + APIParamConstants.tier_id + "=" + customerRegModel.getDevice_tier(), TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {

                try {
                    JSONArray arr = new JSONArray(response);

                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject obj = arr.getJSONObject(i);
                        CustomerRegProductListModel model = new CustomerRegProductListModel();
                        model.setPackage_fee_id(obj.optString("id"));
                        model.setPackage_id(obj.optString("package_id"));
                        model.setPackage_fee(obj.optString("fee"));

                        JSONObject package_obj = obj.optJSONObject("package");
                        JSONObject payment_month_obj = package_obj.optJSONObject("payment_month");
                        model.setPayment_month_validity(payment_month_obj.optString("name"));

                        JSONObject tier_obj = obj.optJSONObject("tier");
                        if (tier_obj != null) {
                            model.setTier_name(tier_obj.optString("name"));
                            model.setTier_description(tier_obj.optString("description"));
                        }

                        product_list.add(model);
                    }
                    adapter.notifyAdapter(product_list);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                err_msg = "";
                String json = null;

                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");

                    show_errors();

                }
            }
        });
    }

    private void show_errors() {
        if (!err_msg.equals("")) {
            Snackbar.make(rl_container, err_msg, Snackbar.LENGTH_LONG).show();
        }
    }
}
