package yakin.iserve.com.yakin.fragments;


import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.activities.CustomerDeviceDetailsActivity;
import yakin.iserve.com.yakin.activities.ExistingCustomerDetailsActivity;
import yakin.iserve.com.yakin.activities.ExistingCustomerFindActivity;
import yakin.iserve.com.yakin.activities.ProductListingActivity;
import yakin.iserve.com.yakin.activities.RegisterCustomerActivity;
import yakin.iserve.com.yakin.activities.PolicyRenewalSearchCustomerActivity;
import yakin.iserve.com.yakin.activities.TrainingMaterialActivity;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.dialogFragments.WalletWarningDialogFragment;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.helper.ProgressBarAnimation;
import yakin.iserve.com.yakin.interfaces.APIUrls;
import yakin.iserve.com.yakin.models.RetailerOrSalesPersonModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, WalletWarningDialogFragment.OnTopupClickListener {

    public static String TAG = "HomeFragment";

    View view;
    RelativeLayout rl_container;


    LinearLayout ll_wallet;
    ProgressBar pb_wallet;

    RelativeLayout rl_reg_customer, rl_policy_renewal, rl_product_list, rl_training_materials;
    RelativeLayout rl_existing_customer;


    RetailerOrSalesPersonModel userModel = new RetailerOrSalesPersonModel();
    int wallet_strength = 0;


    public OnMyWalletClicked walletClickCallBack = null;


    public static NavigationView navigationView;
    TextView tv_user_name_nav_bar;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if(savedInstanceState!=null){
            String t=savedInstanceState.getString("test");
        }
        view = inflater.inflate(R.layout.fragment_home, container, false);

        initViews();

        return view;
    }


    private void initViews() {

        rl_container = (RelativeLayout) view.findViewById(R.id.rl_container);

        ll_wallet = (LinearLayout) view.findViewById(R.id.ll_wallet);
        pb_wallet = (ProgressBar) view.findViewById(R.id.pb_wallet);

        rl_reg_customer = (RelativeLayout) view.findViewById(R.id.rl_reg_customer);
        rl_policy_renewal = (RelativeLayout) view.findViewById(R.id.rl_policy_renewal);
        rl_existing_customer = (RelativeLayout) view.findViewById(R.id.rl_existing_customer);
        rl_product_list = (RelativeLayout) view.findViewById(R.id.rl_product_list);
        rl_training_materials = (RelativeLayout) view.findViewById(R.id.rl_training_materials);

//        navigationView = (NavigationView) view.findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        tv_user_name_nav_bar = (TextView) headerView.findViewById(R.id.tv_username);


        setListeners();
//        setVals();
        //startTimer();
    }

    private void getUserDetails() {


        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(rl_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(getActivity());
        networkRequest.strReqGetWithLoaderWithAccessTokenHeader(getActivity(), APIUrls.user_url, TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {
                manipulateDeviceResponse(response);
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                String json = null;

                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    String err_msg = "";
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    if (!err_msg.equals("")) {
                        Snackbar.make(rl_container, err_msg, Snackbar.LENGTH_LONG).show();
                    }
                }

            }


        });

    }

    private void manipulateDeviceResponse(String response) {
        try {
            JSONObject obj = new JSONObject(response);

            userModel.setId(obj.optString("id"));
            userModel.setName(obj.optString("name"));
            userModel.setUsername(obj.optString("username"));
            userModel.setEmail(obj.optString("email"));
            userModel.setGender_id(obj.optString("gender_id"));
            userModel.setMobile_number(obj.optString("mobile_number"));
            userModel.setIdentification_type_id(obj.optString("identification_type_id"));
            userModel.setIdentification_number(obj.optString("identification_number"));
            userModel.setActivated(obj.optString("activated"));
            userModel.setLast_login(obj.optString("last_login"));
            userModel.setLanguage(obj.optString("language"));
            userModel.setStatus_id(obj.optString("status_id"));
            userModel.setRole_id(obj.optString("role_id"));
            userModel.setImage(obj.optString("image"));
            userModel.setRegion(obj.optString("region"));
            userModel.setReferral_code(obj.optString("referral_code"));
            userModel.setReferral_id(obj.optString("referral_id"));
            userModel.setCreated_at(obj.optString("created_at"));
            userModel.setUpdated_at(obj.optString("updated_at"));
            userModel.setWallet(obj.optString("wallet"));
            userModel.setParent_referral(obj.optString("parent_referral"));

            Paper.book().write(PaperDBConstants.user_model, userModel);

            //setVals();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void getNavigationViewFromParentActivity(NavigationView navigationView) {
        this.navigationView = navigationView;
    }

    private void setVals() {

        String getWallet = userModel.getWallet();

        if (!getWallet.equalsIgnoreCase("null")) {
            if (getWallet != null && getWallet.length() > 0) {
                wallet_strength = (int)Double.parseDouble(userModel.getWallet());
            }
        }

        ProgressBarAnimation anim = new ProgressBarAnimation(pb_wallet, 0, wallet_strength);
        anim.setDuration(1000);
        pb_wallet.startAnimation(anim);


        if (!userModel.getUsername().equals(null)) {
            tv_user_name_nav_bar.setText(userModel.getUsername());
        }

        if (!getWallet.equalsIgnoreCase("null")) {
            if (getWallet != null && getWallet.length() > 0) {
                if (wallet_strength < 20) {

                    WalletWarningDialogFragment walletWarningDialogFragment = new WalletWarningDialogFragment();
                    walletWarningDialogFragment.callBack = this;
                    walletWarningDialogFragment.show(getActivity().getFragmentManager(), WalletWarningDialogFragment.TAG);
                }
            }
        }

//        pb_wallet.setDrawingCacheBackgroundColor(getResources().getColor(R.color.blue));

//        if (wallet_strength >= 70) {
//            pb_wallet.getProgressDrawable().setColorFilter(getResources().getColor(R.color.green), PorterDuff.Mode.SRC_IN);
//
//        }/* else if (wallet_strength < 70 && wallet_strength >= 50) {
//            circle_progress.setFinishedColor(getResources().getColor(R.color.blue));
//        }*/ else if (wallet_strength < 70 && wallet_strength >= 40) {
//            pb_wallet.getProgressDrawable().setColorFilter(getResources().getColor(R.color.yellow), PorterDuff.Mode.SRC_IN);
//
//        } else {
//            pb_wallet.getProgressDrawable().setColorFilter(getResources().getColor(R.color.red), PorterDuff.Mode.SRC_IN);
//
//        }


    }

    private Timer mTimer1;
    private TimerTask mTt1;
    private Handler mTimerHandler = new Handler();

    private void stopTimer() {
        if (mTimer1 != null) {
            mTimer1.cancel();
            mTimer1.purge();
        }
    }

    private void startTimer() {
        mTimer1 = new Timer();
        mTt1 = new TimerTask() {
            public void run() {
                mTimerHandler.post(new Runnable() {
                    public void run() {
                        //TODO
                        wallet_strength--;
                        setVals();
                        if (wallet_strength <= 10) {
                            stopTimer();
                        }
                    }
                });
            }
        };

        mTimer1.schedule(mTt1, 1000, 100);
    }

    private void setListeners() {
        ll_wallet.setOnClickListener(this);

        rl_reg_customer.setOnClickListener(this);
        rl_policy_renewal.setOnClickListener(this);
        rl_existing_customer.setOnClickListener(this);

        rl_product_list.setOnClickListener(this);
        rl_training_materials.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_reg_customer:
                Paper.book().delete(PaperDBConstants.customer_model);
                getActivity().startActivity(new Intent(getActivity(), RegisterCustomerActivity.class));
                break;

            case R.id.rl_policy_renewal:
                getActivity().startActivity(new Intent(getActivity(), PolicyRenewalSearchCustomerActivity.class));
                break;

            case R.id.rl_existing_customer:
                getActivity().startActivity(new Intent(getActivity(), ExistingCustomerFindActivity.class));
                break;

            case R.id.ll_wallet:
//                startActivity(new Intent(getActivity(), MyWalletActivity.class));
                walletClickCallBack.onWalletClicked();
                break;
            case R.id.rl_product_list:
                getActivity().startActivity(new Intent(getActivity(), ProductListingActivity.class));
                break;

            case R.id.rl_training_materials:
                startActivity(new Intent(getActivity(), TrainingMaterialActivity.class));
                break;
        }
    }


//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//
//        Bundle b=new Bundle();
//        b.putBundle("savedScreen", outState);
//    }


    public interface OnMyWalletClicked {
        void onWalletClicked();
    }


    @Override
    public void onResume() {
        super.onResume();
        //api call
        getUserDetails();
        getWallet();
    }


    private void getWallet() {

        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(rl_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(getActivity());
        networkRequest.strReqGetWithLoaderWithAccessTokenHeader(getActivity(), APIUrls.wallet_info, TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {

                try {
                    JSONObject obj = new JSONObject(response);
                    userModel.setWallet(obj.optString("strength"));
                    setVals();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                String json = null;

                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    String err_msg = "";
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    if (!err_msg.equals("")) {
                        Snackbar.make(rl_container, err_msg, Snackbar.LENGTH_LONG).show();
                    }
                }

            }
        });

    }

    @Override
    public void OnTopupClick() {
        walletClickCallBack.onWalletClicked();      // again call another interface for move into the wallet page
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("test","test");//u("navigationView", navigationView);
    }
}
