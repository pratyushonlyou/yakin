package yakin.iserve.com.yakin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.models.MyWalletHistoryModel;
import yakin.iserve.com.yakin.viewHolders.MyWalletHistoryViewHolder;

/**
 * Created by AMIRULSUFI on 1/2/2018.
 */

public class MyWalletHistoryRecyclerViewAdapter extends RecyclerView.Adapter<MyWalletHistoryViewHolder> {

    private Context c;
    private ArrayList<MyWalletHistoryModel> myWalletHistoryModels;


    public MyWalletHistoryRecyclerViewAdapter(Context c, ArrayList<MyWalletHistoryModel> myWalletHistoryModels) {
        this.c = c;
        this.myWalletHistoryModels = myWalletHistoryModels;
    }


    @Override
    public MyWalletHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyWalletHistoryViewHolder(LayoutInflater.from(c).inflate(R.layout.row_wallet_history, parent, false));
    }

    @Override
    public void onBindViewHolder(MyWalletHistoryViewHolder holder, int position) {
        holder.tv_customer_name.setText(myWalletHistoryModels.get(position).getCustomerName());
        holder.tv_date.setText(myWalletHistoryModels.get(position).getDate());
        holder.tv_amount.setText(c.getResources().getString(R.string.currency) + " " + myWalletHistoryModels.get(position).getAmount());
    }

    @Override
    public int getItemCount() {
        return myWalletHistoryModels.size();
    }
}
