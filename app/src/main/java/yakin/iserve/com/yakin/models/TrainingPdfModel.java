package yakin.iserve.com.yakin.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AMIRULSUFI on 1/5/2018.
 */

public class TrainingPdfModel implements Parcelable {

    private String pdf_id = "";
    private String pdf_link = "";
    private String pdf_img = "";
    private String pdf_uolpaded_by = "";
    private String pdf_uploaded_date = "";
    private String desc = "";



    public TrainingPdfModel(String link, String img, String uploaded_by, String date, String desc){
        this.pdf_link=link;
        this.pdf_img=img;
        this.pdf_uolpaded_by=uploaded_by;
        this.pdf_uploaded_date=date;
        this.desc=desc;
    }



    protected TrainingPdfModel(Parcel in) {
        pdf_id = in.readString();
        pdf_link = in.readString();
        pdf_img = in.readString();
        pdf_uolpaded_by = in.readString();
        pdf_uploaded_date = in.readString();
        desc = in.readString();
    }

    public static final Creator<TrainingPdfModel> CREATOR = new Creator<TrainingPdfModel>() {
        @Override
        public TrainingPdfModel createFromParcel(Parcel in) {
            return new TrainingPdfModel(in);
        }

        @Override
        public TrainingPdfModel[] newArray(int size) {
            return new TrainingPdfModel[size];
        }
    };

    public String getPdf_id() {
        return pdf_id;
    }

    public void setPdf_id(String pdf_id) {
        this.pdf_id = pdf_id;
    }

    public String getPdf_link() {
        return pdf_link;
    }

    public void setPdf_link(String pdf_link) {
        this.pdf_link = pdf_link;
    }

    public String getPdf_img() {
        return pdf_img;
    }

    public void setPdf_img(String pdf_img) {
        this.pdf_img = pdf_img;
    }

    public String getPdf_uolpaded_by() {
        return pdf_uolpaded_by;
    }

    public void setPdf_uolpaded_by(String pdf_uolpaded_by) {
        this.pdf_uolpaded_by = pdf_uolpaded_by;
    }

    public String getPdf_uploaded_date() {
        return pdf_uploaded_date;
    }

    public void setPdf_uploaded_date(String pdf_uploaded_date) {
        this.pdf_uploaded_date = pdf_uploaded_date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pdf_id);
        dest.writeString(pdf_link);
        dest.writeString(pdf_img);
        dest.writeString(pdf_uolpaded_by);
        dest.writeString(pdf_uploaded_date);
        dest.writeString(desc);
    }
}
