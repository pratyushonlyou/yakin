package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.OnActivityResultConstants;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.dialogFragments.CustomCalendarDialogFragment;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.interfaces.APIParamConstants;
import yakin.iserve.com.yakin.interfaces.APIUrls;
import yakin.iserve.com.yakin.models.CustomerRegModel;

public class ManualEntryActivity extends AppCompatActivity implements View.OnClickListener, CustomCalendarDialogFragment.OnDateSelect {


    public static String TAG = "ManualEntryActivity";
    LinearLayout ll_container;
    ImageView iv_customer_img;

    TextInputLayout til_name, til_address, til_ic, til_email, til_ph;
    EditText et_user_fname, et_user_lname, et_user_address, et_user_ic, et_user_dob, et_user_email, et_user_phone_no;
    String str_name = "", str_address = "", str_ic = "", str_email = "", str_ph = "";
    String err_name = "", err_address = "", err_ic = "", err_email = "", err_ph = "";
    String err_msg = "";

    Button btn_nxt;

    String dob_str = "";
    CustomerRegModel customerRegModel = new CustomerRegModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // If we have a saved state then we can restore it now
        if (savedInstanceState != null) {
            img_path = savedInstanceState.getString(STATE_COUNTER);
        }

        if (getIntent().getExtras() != null && getIntent().getExtras().getParcelable("customerRegModel") != null) {
            customerRegModel = getIntent().getExtras().getParcelable("customerRegModel");
            Paper.book().write(PaperDBConstants.customer_model, customerRegModel);
        } else if (Paper.book().read(PaperDBConstants.customer_model) != null) {
            customerRegModel = Paper.book().read(PaperDBConstants.customer_model);
        }


        setContentView(R.layout.activity_manual_entry);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.manual_entry));


        initViews();
        setVals();

    }

    private void setVals() {
        if (!img_path.equals(null) && !img_path.equals("")) {
            iv_customer_img.setImageURI(null);
            iv_customer_img.setImageURI(Uri.parse(img_path));
        }

        // setting values coming from previous activity
        if (customerRegModel != null) {
            if (!customerRegModel.getName().equals(null) && !customerRegModel.getName().equals("")) {
                et_user_fname.setText(customerRegModel.getName());
            }
            if (customerRegModel.getAddress() != null && !customerRegModel.getAddress().equals("")) {
                et_user_address.setText(customerRegModel.getAddress());
            }
            if (customerRegModel.getIdentification_number() != null && !customerRegModel.getIdentification_number().equals("")) {
                et_user_ic.setText(customerRegModel.getIdentification_number());
            }

        }
    }

    private void initViews() {
        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        iv_customer_img = (ImageView) findViewById(R.id.iv_customer_img);
        et_user_fname = (EditText) findViewById(R.id.et_user_fname);
        et_user_lname = (EditText) findViewById(R.id.et_user_lname);
        et_user_address = (EditText) findViewById(R.id.et_user_address);
        et_user_ic = (EditText) findViewById(R.id.et_user_ic);
        et_user_dob = (EditText) findViewById(R.id.et_user_dob);
        et_user_email = (EditText) findViewById(R.id.et_user_email);
        et_user_phone_no = (EditText) findViewById(R.id.et_user_phone_no);
        btn_nxt = (Button) findViewById(R.id.btn_nxt);

        til_name = (TextInputLayout) findViewById(R.id.til_name);
        til_address = (TextInputLayout) findViewById(R.id.til_address);
        til_ic = (TextInputLayout) findViewById(R.id.til_ic);
        til_email = (TextInputLayout) findViewById(R.id.til_email);
        til_ph = (TextInputLayout) findViewById(R.id.til_ph);

        setListeners();
    }


    private void setListeners() {
        iv_customer_img.setOnClickListener(this);

        et_user_dob.setClickable(true);
        et_user_dob.setOnClickListener(this);

        btn_nxt.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_customer_img:
                if (PermissionsCheck.checkCameraPermission(ManualEntryActivity.this)) {

                    startActivityForResult(new Intent(ManualEntryActivity.this, CustomerImageSnapActivity.class), OnActivityResultConstants.FACE_DETECTOR_FROM_MANUAL_ENTRY);

                } else {
                    PermissionsCheck.requestCameraPermission(ManualEntryActivity.this);
                }
                break;

            case R.id.et_user_dob:
                CustomCalendarDialogFragment customCalendarDialogFragment = new CustomCalendarDialogFragment();
                customCalendarDialogFragment.callBack = this;
                customCalendarDialogFragment.show(getFragmentManager(), CustomCalendarDialogFragment.TAG);

                break;
            case R.id.btn_nxt:
                // clear the previous errors
                til_name.setError(null);
                til_address.setError(null);
                til_ic.setError(null);
                til_email.setError(null);
                til_ph.setError(null);
                if (validate()) {
                // api call
                    doRegistration();
                }

                break;
        }
    }

    private boolean validate() {
        str_name = et_user_fname.getText().toString().trim();
        str_address = et_user_address.getText().toString().trim();
        str_ic = et_user_ic.getText().toString().trim();
        str_email = et_user_email.getText().toString().trim();
        str_ph = et_user_phone_no.getText().toString().trim();

        if (str_name.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.name_empty), Snackbar.LENGTH_LONG).show();
            et_user_fname.requestFocus();
            return false;
        }
        if (str_address.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.address_empty), Snackbar.LENGTH_LONG).show();
            et_user_address.requestFocus();
            return false;
        }
        if (str_ic.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.ic_empty), Snackbar.LENGTH_LONG).show();
            et_user_ic.requestFocus();
            return false;
        }
        if (str_email.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.email_empty), Snackbar.LENGTH_LONG).show();
            et_user_email.requestFocus();
            return false;
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(str_email).matches()) {
            Snackbar.make(ll_container, getResources().getString(R.string.email_invalid), Snackbar.LENGTH_LONG).show();
            et_user_email.requestFocus();
            return false;
        }
        if (str_ph.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.ph_empty), Snackbar.LENGTH_LONG).show();
            et_user_phone_no.requestFocus();
            return false;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == OnActivityResultConstants.FACE_DETECTOR_FROM_MANUAL_ENTRY) {
                Log.e("image", String.valueOf(data));
                img_path = (String) data.getExtras().get("img_path");

                iv_customer_img.setImageURI(null);
                iv_customer_img.setImageURI(Uri.parse(img_path));
            }
        }
    }


    String img_path = "";
    private static final String STATE_COUNTER = "view_state";

    private Bundle state_bundle;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Make sure to call the super method so that the states of our views are saved
        super.onSaveInstanceState(outState);
        // Save our own state now
        outState.putString(STATE_COUNTER, img_path);
    }

    @Override
    public void onDateSelect(String type, String month_name, String dayOfWeek, int year, int month, int dayOfMonth, Calendar c_selected) {
        String y, m, d;
        if (dayOfMonth <= 9) {
            d = "0" + dayOfMonth;
        } else {
            d = dayOfMonth + "";
        }


        dob_str = d + "-" + month_name + "-" + year;
        et_user_dob.setText(d + "-" + month_name + "-" + year);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    private void doRegistration() {
        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(ll_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIParamConstants.name, str_name);
        hashMap.put(APIParamConstants.address, str_address);
        hashMap.put(APIParamConstants.identification_number, str_ic);
        hashMap.put(APIParamConstants.email, str_email);
        hashMap.put(APIParamConstants.mobile_number, str_ph);
        hashMap.put(APIParamConstants.role_id, APIParamConstants.customer_role_id);    // 7 -> retailer, 9 -> customer
        hashMap.put(APIParamConstants.identification_type_id, APIParamConstants.identification_national_type);


        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqPostWithLoader(this, APIUrls.customer_reg_url, TAG, hashMap, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {


                try {
                    JSONObject obj = new JSONObject(response);

                    customerRegModel.setId(obj.optString("id"));
                    customerRegModel.setName(obj.optString("name"));
                    customerRegModel.setUsername(obj.optString("username"));
                    customerRegModel.setEmail(obj.optString("email"));
                    customerRegModel.setAddress(obj.optString("address"));
                    customerRegModel.setPhone_no(obj.optString("mobile_number"));
                    customerRegModel.setIdentification_type_id(obj.optString("identification_type_id"));
                    customerRegModel.setIdentification_number(obj.optString("identification_number"));
                    customerRegModel.setLanguage(String.valueOf(obj.optInt("language")));

                    Paper.book().write(PaperDBConstants.customer_model, customerRegModel);
                    Paper.book().write(PaperDBConstants.customer_reg_last_accessed_page, PaperDBConstants.customer_reg_manual_entry);

                    Bundle b = new Bundle();
                    b.putParcelable("customerRegModel", customerRegModel);
                    Intent i = new Intent(ManualEntryActivity.this, CustomerDeviceDetailsActivity.class);
                    i.putExtras(b);
                    startActivity(i);
//                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                err_name = "";
                err_address = "";
                err_ic = "";
                err_email = "";
                err_ph = "";
                err_msg = "";


                String json = null;


                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    err_name = HandleErrorMsgFromVolley.trimMessage(json, "name");
                    err_address = HandleErrorMsgFromVolley.trimMessage(json, "address");
                    err_email= HandleErrorMsgFromVolley.trimMessage(json, "email");
                    err_ic = HandleErrorMsgFromVolley.trimMessage(json, "identification_number");
                    err_ph = HandleErrorMsgFromVolley.trimMessage(json, "mobile_number");


                    show_errors();


                }

            }

        });
    }

    private void show_errors() {
        if (!err_msg.equals("")) {
            Snackbar.make(ll_container, err_msg, Snackbar.LENGTH_LONG).show();
        }
        if (!err_name.equals("")) {
            til_name.setError(err_name);
            et_user_fname.requestFocus();
        }
        if (!err_address.equals("")) {
            til_address.setError(err_address);
            et_user_address.requestFocus();
        }
        if (!err_ic.equals("")) {
            til_ic.setError(err_ic);
            et_user_ic.requestFocus();
        }
        if (!err_email.equals("")) {
            til_email.setError(err_email);
            et_user_email.requestFocus();
        }
        if (!err_ph.equals("")) {
            til_ph.setError(err_ph);
            et_user_phone_no.requestFocus();
        }

    }
}
