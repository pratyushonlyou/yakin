package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.models.CustomerRegModel;

public class PolicyRenewalThankYouActivity extends AppCompatActivity implements View.OnClickListener {


    RelativeLayout rl_thankyou_container;
    Button btn_home;



    CustomerRegModel customerRegModel = new CustomerRegModel();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy_renewal_thank_you);


        if (getIntent().getExtras().getParcelable("customerRegModel") != null) {
            customerRegModel = getIntent().getExtras().getParcelable("customerRegModel");
        }


        rl_thankyou_container = (RelativeLayout) findViewById(R.id.rl_thankyou_container);
        btn_home = (Button) findViewById(R.id.btn_home);
        btn_home.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_home:
                startActivity(new Intent(PolicyRenewalThankYouActivity.this, DashBoardContainerActivity.class));
                ActivityCompat.finishAffinity(PolicyRenewalThankYouActivity.this);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // do nothing, prevent to go back to the payment page
        Snackbar.make(rl_thankyou_container, getResources().getString(R.string.cant_go_back), Snackbar.LENGTH_LONG).show();
    }



}
