package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.fragments.HomeFragment;
import yakin.iserve.com.yakin.fragments.MyWalletFragment;
import yakin.iserve.com.yakin.models.RetailerOrSalesPersonModel;

public class DashBoardContainerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.OnMyWalletClicked {


    ConstraintLayout cl_container;
    boolean doubleBackToExitPressedOnce = false;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        cl_container = (ConstraintLayout) findViewById(R.id.cl_container);


        HomeFragment homeFragment = new HomeFragment();
        homeFragment.getNavigationViewFromParentActivity(navigationView);
        homeFragment.walletClickCallBack = this;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.cl_container, homeFragment, HomeFragment.TAG)
                .addToBackStack(HomeFragment.TAG)
                .commit();

        // to set selected menu item if back button is pressed
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                FragmentManager fm = getSupportFragmentManager();
                String name = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                switch (name) {
                    case "HomeFragment":
                        navigationView.setCheckedItem(R.id.nav_home);
                        setTitle(getResources().getString(R.string.home));
                        break;
                    case "MyWalletFragment":
                        navigationView.setCheckedItem(R.id.nav_wallet);
                        setTitle(getResources().getString(R.string.my_wallet_header));
                        break;

                }
//                if (!TextUtils.isEmpty(name))
//                    selectNavigationDrawerItem(Integer.parseInt(name));.


            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                // close app
                if (doubleBackToExitPressedOnce) {
                    //super.onBackPressed();
                    finish();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Snackbar.make(cl_container, getResources().getString(R.string.click_again_to_exit), Snackbar.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
                // in my case I get the support fragment manager, it should work with the native one too
//                FragmentManager fragmentManager = getSupportFragmentManager();
                // this will clear the back stack and displays no animation on the screen
//                fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getFragmentManager().popBackStack();

//                BookHotelNRoomStayViewPagerFragment bookHotelNRoomStayViewPagerFragment = new BookHotelNRoomStayViewPagerFragment();
//                getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.container, bookHotelNRoomStayViewPagerFragment, BookHotelRoomFragment.TAG)
//                        .addToBackStack(BookHotelRoomFragment.TAG)
//                        .commit();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            HomeFragment homeFragment = new HomeFragment();
            homeFragment.getNavigationViewFromParentActivity(navigationView);
            homeFragment.walletClickCallBack = this;
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.cl_container, homeFragment, HomeFragment.TAG)
                    .addToBackStack(HomeFragment.TAG)
                    .commit();
        } else if (id == R.id.nav_wallet) {
            // wallet page
            MyWalletFragment myWalletFragment = new MyWalletFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.cl_container, myWalletFragment, MyWalletFragment.TAG)
                    .addToBackStack(MyWalletFragment.TAG)
                    .commit();
//            startActivity(new Intent(DashBoardContainerActivity.this, MyWalletActivity.class));
        } else if (id == R.id.nav_logout) {
            Paper.book().destroy();
            startActivity(new Intent(DashBoardContainerActivity.this, SignInLandingActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (Paper.book().read(PaperDBConstants.customer_model) != null) {       // that means, customer reg was in process

//            startActivity(new Intent(DashBoardContainerActivity.this, ManualEntryActivity.class));

        }
    }


    @Override
    public void onWalletClicked() {
        MyWalletFragment myWalletFragment = new MyWalletFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.cl_container, myWalletFragment, MyWalletFragment.TAG)
                .addToBackStack(MyWalletFragment.TAG)
                .commit();
    }

}
