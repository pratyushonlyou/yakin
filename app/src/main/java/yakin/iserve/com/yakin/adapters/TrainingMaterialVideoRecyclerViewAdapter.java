package yakin.iserve.com.yakin.adapters;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.activities.VideoViewActivity;
import yakin.iserve.com.yakin.custom_views.ScalableVideoView;
import yakin.iserve.com.yakin.models.TrainingVideoModel;
import yakin.iserve.com.yakin.viewHolders.TrainingVideoViewHolder;

/**
 * Created by AMIRULSUFI on 1/4/2018.
 */

public class TrainingMaterialVideoRecyclerViewAdapter extends RecyclerView.Adapter<TrainingVideoViewHolder> implements ScalableVideoView.OnVideoPlayBackListener {

    public static String TAG = "TrainingMaterialVideoRecyclerViewAdapter";
    Context c;
    ArrayList<TrainingVideoModel> trainingVideoModels;

    public TrainingMaterialVideoRecyclerViewAdapter(Context c, ArrayList<TrainingVideoModel> trainingVideoModels) {
        this.c = c;
        this.trainingVideoModels = trainingVideoModels;
    }


    @Override
    public TrainingVideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TrainingVideoViewHolder(LayoutInflater.from(c).inflate(R.layout.row_training_video, parent, false));
    }

    @Override
    public void onBindViewHolder(final TrainingVideoViewHolder holder, final int position) {
        holder.vv.setVideoURI(Uri.parse(trainingVideoModels.get(position).getVideo_url()));
        holder.tv_posted_by.setText(trainingVideoModels.get(position).getVideo_posted_by());
        holder.tv_posted_date.setText(trainingVideoModels.get(position).getVideo_posted_date());
        holder.tv_desc.setText(trainingVideoModels.get(position).getVideo_desc());

        holder.vv.requestFocus();
        holder.vv.setListener(this);
        holder.vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                holder.vv.seekTo(100);
                holder.progressBar.setVisibility(View.GONE);
//                holder.ivPlay.setVisibility(View.VISIBLE);
//                holder.rl_play_btn_container.setVisibility(View.VISIBLE);
            }
        });
        holder.vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
//                holder.ivPlay.setVisibility(View.VISIBLE);
//                holder.rl_play_btn_container.setVisibility(View.VISIBLE);

            }
        });

        holder.rl_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                holder.ivPlay.setVisibility(View.GONE);
//                holder.rl_play_btn_container.setVisibility(View.GONE);


                Bundle b=new Bundle();
                b.putParcelable("videoModel",trainingVideoModels.get(position));

                Intent i=new Intent(c, VideoViewActivity.class);
                i.putExtras(b);
                c.startActivity(i);

//                android.app.FragmentTransaction ft = ((TrainingMaterialActivity)c).getFragmentManager().beginTransaction();
//                TrainingVideoDialogFragment dialogFragment = new TrainingVideoDialogFragment();
//
//                dialogFragment.passModelValue(c, trainingVideoModels.get(position));
                // dialogFragment.show(ft,TAG);



//                MediaController vidControl = new MediaController(c);
//                vidControl.setAnchorView(holder.vv);
//                holder.vv.setMediaController(vidControl);
//
//                holder.vv.start();
            }
        });

//        holder.rl_total.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                android.app.FragmentTransaction ft = ((TrainingMaterialActivity)c).getFragmentManager().beginTransaction();
//                TrainingVideoDialogFragment dialogFragment = new TrainingVideoDialogFragment();
//
//                dialogFragment.passModelValue(c, trainingVideoModels.get(position));
////                dialogFragment.show(ft,TAG);
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return trainingVideoModels.size();
    }

    @Override
    public void onVideoPlay() {

    }

    @Override
    public void onVideoPause(int position) {

    }
}
