package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iservetech.documentocr.DocumentScannerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.models.CustomerRegModel;

public class ExistingCustomerFindActivity extends AppCompatActivity implements View.OnClickListener {


    RelativeLayout rl_read_mykad, rl_read_passport;
    CustomerRegModel customerRegModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_customer_find);

        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.existing_customer));


        initViews();
    }

    private void initViews() {
        rl_read_mykad = (RelativeLayout) findViewById(R.id.rl_read_mykad);
        rl_read_passport = (RelativeLayout) findViewById(R.id.rl_read_passport);


        setListeners();
    }

    private void setListeners() {
        rl_read_mykad.setOnClickListener(this);
        rl_read_passport.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_read_mykad:
                if (PermissionsCheck.checkCameraPermission(ExistingCustomerFindActivity.this)) {
//                    takePhoto();
                    //startActivityForResult(new Intent(RegisterCustomerActivity.this, MyKadOcrCaptureActivity.class), OnActivityResultConstants.RC_OCR_CAPTURE);
                    DocumentScannerActivity.readMyKad(ExistingCustomerFindActivity.this, new DocumentScannerActivity.DocumentScannerListener() {
                        @Override
                        public void onOcrCompleted(String s) {
                            Log.d("tag", s);
                            String test = "";
//                            OpenCVLoader
                            try {
                                JSONObject obj = new JSONObject(s);
                                String address = "";
                                address = obj.optString("address").replace("\n", ",");

                                customerRegModel = new CustomerRegModel();
                                customerRegModel.setName(obj.optString("name"));
                                customerRegModel.setAddress(address);
                                customerRegModel.setIdentification_number(obj.optString("icNum"));
                                customerRegModel.setGender(obj.optString("gender"));


                                Bundle b = new Bundle();
                                b.putParcelable("customerRegModel", customerRegModel);
                                Intent i = new Intent(ExistingCustomerFindActivity.this, ExistingCustomerDetailsActivity.class);
                                i.putExtras(b);
                                startActivity(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                } else {
                    PermissionsCheck.requestCameraPermission(ExistingCustomerFindActivity.this);
                }
                break;
            case R.id.rl_read_passport:

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
