package yakin.iserve.com.yakin.models;

/**
 * Created by AMIRULSUFI on 1/3/2018.
 */

public class DeviceTierModel {

    private String tier_id="";
    private String tier_name="";
    private String tier_range="";

    public DeviceTierModel(String id, String name,String tier_range){
        this.tier_id=id;
        this.tier_name = name;
        this.tier_range=tier_range;
    }


    public String getTier_name() {
        return tier_name;
    }

    public void setTier_name(String tier_name) {
        this.tier_name = tier_name;
    }

    public String getTier_range() {
        return tier_range;
    }

    public void setTier_range(String tier_range) {
        this.tier_range = tier_range;
    }

    public String getTier_id() {
        return tier_id;
    }

    public void setTier_id(String tier_id) {
        this.tier_id = tier_id;
    }
}
