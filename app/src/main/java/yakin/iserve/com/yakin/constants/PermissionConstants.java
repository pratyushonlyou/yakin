package yakin.iserve.com.yakin.constants;

/**
 * Created by AMIRULSUFI on 12/8/2017.
 */

public interface PermissionConstants {

    int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
    int MY_PERMISSIONS_READ_PHONE_STATE = 2;
}
