package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.models.CustomerRegModel;

public class PolicyRenewalConfirmActivity extends AppCompatActivity implements View.OnClickListener {


    Button btn_confirm;
    CustomerRegModel customerRegModel = new CustomerRegModel();

    TextView tv_imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy_renewal_confirm);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.app_name));

        if (getIntent().getExtras().getParcelable("customerRegModel") != null) {
            customerRegModel = getIntent().getExtras().getParcelable("customerRegModel");
        }




        initViews();

    }

    private void initViews() {
        tv_imei = (TextView)findViewById(R.id.tv_imei);
        btn_confirm = (Button) findViewById(R.id.btn_confirm);


        setVals();
        setListeners();

    }

    private void setVals() {
        if(customerRegModel.getImei_no()!=null || !customerRegModel.getImei_no().equals("")){
            tv_imei.setText(customerRegModel.getImei_no());
        }
    }

    private void setListeners() {
        btn_confirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:

                Bundle b = new Bundle();
                b.putParcelable("customerRegModel",customerRegModel);
                Intent i=new Intent(PolicyRenewalConfirmActivity.this, PolicyRenewalCustomerSignatureActivity.class);
                i.putExtras(b);
                startActivity(i);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
