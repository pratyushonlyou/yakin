package yakin.iserve.com.yakin.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by AMIRULSUFI on 12/22/2017.
 */

public class CustomerRegModel implements Parcelable {

    private String id="";
    private String name="";
    private String username="";
    private String address="";
    private String identification_type_id ="";
    private String identification_number ="";
    private String email = "";
    private String phone_no="";
    private String gender="";
    private String language="";

    private String make="";
    private String model="";
    private String device_color="";
    private String tier="";
    private String imei_no="";
    private String imei_no2="";

    //private String imei_no_extra="";
    private String other_model="";
    private String device_tier="";


    private String device_purchase_date="";
    CustomerRegProductListModel customerRegProductListModel = new CustomerRegProductListModel();

    public ArrayList<DevicesModel> devicesModels = new ArrayList<>();


    public CustomerRegModel() {

    }

    protected CustomerRegModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        username = in.readString();
        address = in.readString();
        identification_type_id = in.readString();
        identification_number = in.readString();
        email = in.readString();
        phone_no = in.readString();
        gender = in.readString();
        language = in.readString();
        make = in.readString();
        model = in.readString();
        device_color = in.readString();
        tier = in.readString();
        imei_no = in.readString();
        imei_no2 = in.readString();
        other_model = in.readString();
        device_tier = in.readString();
        device_purchase_date = in.readString();
        devicesModels = in.createTypedArrayList(DevicesModel.CREATOR);
    }

    public static final Creator<CustomerRegModel> CREATOR = new Creator<CustomerRegModel>() {
        @Override
        public CustomerRegModel createFromParcel(Parcel in) {
            return new CustomerRegModel(in);
        }

        @Override
        public CustomerRegModel[] newArray(int size) {
            return new CustomerRegModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdentification_type_id() {
        return identification_type_id;
    }

    public void setIdentification_type_id(String identification_type_id) {
        this.identification_type_id = identification_type_id;
    }

    public String getIdentification_number() {
        return identification_number;
    }

    public void setIdentification_number(String identification_number) {
        this.identification_number = identification_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDevice_color() {
        return device_color;
    }

    public void setDevice_color(String device_color) {
        this.device_color = device_color;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getImei_no() {
        return imei_no;
    }

    public void setImei_no(String imei_no) {
        this.imei_no = imei_no;
    }

    public String getImei_no2() {
        return imei_no2;
    }

    public void setImei_no2(String imei_no2) {
        this.imei_no2 = imei_no2;
    }

    public String getOther_model() {
        return other_model;
    }

    public void setOther_model(String other_model) {
        this.other_model = other_model;
    }

    public String getDevice_tier() {
        return device_tier;
    }

    public void setDevice_tier(String device_tier) {
        this.device_tier = device_tier;
    }

    public String getDevice_purchase_date() {
        return device_purchase_date;
    }

    public void setDevice_purchase_date(String device_purchase_date) {
        this.device_purchase_date = device_purchase_date;
    }

    public CustomerRegProductListModel getCustomerRegProductListModel() {
        return customerRegProductListModel;
    }

    public void setCustomerRegProductListModel(CustomerRegProductListModel customerRegProductListModel) {
        this.customerRegProductListModel = customerRegProductListModel;
    }

    public ArrayList<DevicesModel> getDevicesModels() {
        return devicesModels;
    }

    public void setDevicesModels(ArrayList<DevicesModel> devicesModels) {
        this.devicesModels = devicesModels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(username);
        dest.writeString(address);
        dest.writeString(identification_type_id);
        dest.writeString(identification_number);
        dest.writeString(email);
        dest.writeString(phone_no);
        dest.writeString(gender);
        dest.writeString(language);
        dest.writeString(make);
        dest.writeString(model);
        dest.writeString(device_color);
        dest.writeString(tier);
        dest.writeString(imei_no);
        dest.writeString(imei_no2);
        dest.writeString(other_model);
        dest.writeString(device_tier);
        dest.writeString(device_purchase_date);
        dest.writeTypedList(devicesModels);
    }
}
