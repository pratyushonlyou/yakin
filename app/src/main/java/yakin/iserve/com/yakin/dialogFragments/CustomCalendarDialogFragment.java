package yakin.iserve.com.yakin.dialogFragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by AMIRULSUFI on 12/6/2017.
 */

public class CustomCalendarDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public OnDateSelect callBack;

    public static String type = "";
    public static String TAG = "CustomCalendarDialogFragment";

    public static int year, month, dayOfMonth;
    public static Calendar c;

    public static CustomCalendarDialogFragment newInstance(String _type, int _year, int _month, int _dayOfMonth, Calendar _calendar) {
        CustomCalendarDialogFragment customCalendarDialogFragment = new CustomCalendarDialogFragment();
        type = _type;
        year = _year;
        month = _month;
        dayOfMonth = _dayOfMonth;
        c = _calendar;
        return customCalendarDialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
//        dialog.getDatePicker().set

        Calendar c_max = Calendar.getInstance();
        c_max.set(year, month, day, 0, 0);



        dialog.getDatePicker().setMaxDate(c_max.getTimeInMillis());
        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


        Date date = new Date(year, month, dayOfMonth - 1);

        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(date);

        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE");
        String dayOfWeek = simpledateformat.format(date);


        Calendar c_selected = Calendar.getInstance();
        c_selected.set(Calendar.YEAR, year);
        c_selected.set(Calendar.MONTH, month);
        c_selected.set(Calendar.DAY_OF_MONTH, year);

        callBack.onDateSelect(type, month_name, dayOfWeek, year, month, dayOfMonth, c_selected);
    }


    public interface OnDateSelect {
        void onDateSelect(String type, String month_name, String dayOfWeek, int year, int month, int dayOfMonth, Calendar c_selected);
    }
}
