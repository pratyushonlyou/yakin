package yakin.iserve.com.yakin.helper;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.LinearLayout;

import yakin.iserve.com.yakin.R;

/**
 * Created by DELL on 1/1/2018.
 */

public class CheckValues{

    public static boolean checkIMEINo(Context c, View ll_container, String imei_code) {
        if (imei_code.length() == 15) {
            try {
                char[] imei_str_arr = imei_code.toCharArray();
                int[] imei_digits_arr = new int[15];
                for (int i = 0; i < imei_str_arr.length; i++) {
                    imei_digits_arr[i] = Integer.parseInt(String.valueOf(imei_str_arr[i]));
                }
                int sum_of_digits = 0;
                for (int j = 0; j < imei_digits_arr.length - 1; j++) {
                    if (j % 2 == 0) {         // i.e odd position as position starts with 0
                        sum_of_digits = sum_of_digits + imei_digits_arr[j];
                    } else {
                        int digits_sum = 0;
                        int double_value = imei_digits_arr[j] * 2;

                        if (double_value > 9) {     // means, two digits number
                            int first_digit = double_value % 10;
                            int second_digit = double_value / 10;
                            digits_sum = first_digit + second_digit;
                        } else {
                            digits_sum = double_value;
                        }

                        sum_of_digits = sum_of_digits + digits_sum;
                    }
                }

                int checksum = 0;
                int quotient = sum_of_digits / 10;
                int reminder = sum_of_digits % 10;
                if (reminder > 0) {
                    checksum = 10 - reminder;
                }

                if (checksum != imei_digits_arr[14]) {
                    Snackbar.make(ll_container, c.getResources().getString(R.string.incorrect_imei_no), Snackbar.LENGTH_LONG).show();
                    return false;
                } else {
//                    Snackbar.make(ll_container, "Perfect IMEI no", Snackbar.LENGTH_LONG).show();
                    return true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Snackbar.make(ll_container, c.getResources().getString(R.string.not_an_imei_no), Snackbar.LENGTH_LONG).show();
            return false;
        }
        return false;
    }
}
