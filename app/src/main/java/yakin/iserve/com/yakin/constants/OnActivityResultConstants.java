package yakin.iserve.com.yakin.constants;

/**
 * Created by AMIRULSUFI on 12/12/2017.
 */

public interface OnActivityResultConstants {

    int FACE_DETECTOR_FROM_MANUAL_ENTRY = 101;
    int IMEI_SCAN_REQUEST_CODE = 201;
    int IMEI_SCAN_REQUEST_CODE_EXTRA = 202;

    int RC_OCR_CAPTURE = 9003;

    int TAKE_PICTURE = 101;

}
