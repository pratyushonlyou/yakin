package yakin.iserve.com.yakin.models;

/**
 * Created by AMIRULSUFI on 1/2/2018.
 */

public class MyWalletHistoryModel {

    private String id = "", customerName = "", date = "", amount = "";


    public MyWalletHistoryModel(String id, String name, String date, String amount) {
        this.id = id;
        this.customerName = name;
        this.date = date;
        this.amount = amount;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
