package yakin.iserve.com.yakin.interfaces;

/**
 * Created by AMIRULSUFI on 12/28/2017.
 */

public interface APIParamConstants {

    // common
    String others="others";


    // registration
    String name = "name";
    String username = "username";
    String password = "password";
    String password_confirmation = "password_confirmation";
    String device_uid = "device_uid";

    String role_id = "role_id";
    String retailer_role_id = "7";
    String customer_role_id = "9";

    String region = "region";

    // login
    String cid = "cid";
    String cid_value = "2";
    String cp = "cp";
    String cp_value = "iJ2eHNHgPT8tPulcCkbUOnVFCUgyOtt7z8fAA9dm";


    // devices
    String Authorization = "Authorization";
    String Bearer = "Bearer";


    // customer reg
    String identification_national_type = "51";
    String identification_passport_type = "52";
    String identification_type_id = "identification_type_id";
    String identification_number = "identification_number";
    String mobile_number = "mobile_number";
    String email = "email";
    String address = "address";

    // user device add
    String sku_id = "sku_id";
    String device_purchased_date = "device_purchased_date";
    String customer_id = "customer_id";
    String imei = "imei";
    String imei_2 = "imei_2";
    String model = "model";
    String make = "make";
    String other_model = "other_model";
    String tier_id = "tier_id";


}
