package yakin.iserve.com.yakin.models;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class DeviceCompanyModelColorsModel {


    String color_id, color_name, color_code;

    public String getColor_id() {
        return color_id;
    }

    public void setColor_id(String color_id) {
        this.color_id = color_id;
    }

    public String getColor_name() {
        return color_name;
    }

    public void setColor_name(String color_name) {
        this.color_name = color_name;
    }

    public String getColor_code() {
        return color_code;
    }

    public void setColor_code(String color_code) {
        this.color_code = color_code;
    }
}
