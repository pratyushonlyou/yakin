package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import yakin.iserve.com.yakin.R;


public class PaymentGateWayActivity extends AppCompatActivity  {


    WebView wv_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_gate_way);


        wv_payment = (WebView)findViewById(R.id.wv_payment);
        wv_payment.getSettings().setJavaScriptEnabled(true);

        wv_payment.loadUrl("https://www.upay2us.com/iServeGateway/transaction_window");
    }




    // toolbar menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.payment_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_ok) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
