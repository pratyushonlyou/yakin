package yakin.iserve.com.yakin.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AMIRULSUFI on 1/4/2018.
 */

public class TrainingVideoModel implements Parcelable {

    private String video_id = "";
    private String video_url = "";
    private String video_posted_by = "";
    private String video_posted_date = "";
    private String video_desc = "";

    public TrainingVideoModel(String id, String url, String video_posted_by, String video_posted_date, String video_desc) {
        this.video_id = id;
        this.video_url = url;
        this.video_posted_by = video_posted_by;
        this.video_posted_date = video_posted_date;
        this.video_desc = video_desc;
    }

    protected TrainingVideoModel(Parcel in) {
        video_id = in.readString();
        video_url = in.readString();
        video_posted_by = in.readString();
        video_posted_date = in.readString();
        video_desc = in.readString();
    }

    public static final Creator<TrainingVideoModel> CREATOR = new Creator<TrainingVideoModel>() {
        @Override
        public TrainingVideoModel createFromParcel(Parcel in) {
            return new TrainingVideoModel(in);
        }

        @Override
        public TrainingVideoModel[] newArray(int size) {
            return new TrainingVideoModel[size];
        }
    };

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_posted_by() {
        return video_posted_by;
    }

    public void setVideo_posted_by(String video_posted_by) {
        this.video_posted_by = video_posted_by;
    }

    public String getVideo_desc() {
        return video_desc;
    }

    public void setVideo_desc(String video_desc) {
        this.video_desc = video_desc;
    }

    public String getVideo_posted_date() {
        return video_posted_date;
    }

    public void setVideo_posted_date(String video_posted_date) {
        this.video_posted_date = video_posted_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(video_id);
        dest.writeString(video_url);
        dest.writeString(video_posted_by);
        dest.writeString(video_posted_date);
        dest.writeString(video_desc);
    }
}
