package yakin.iserve.com.yakin.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import yakin.iserve.com.yakin.R;


public class AppUtilities {
    private static Context context = null;

    /**
     * Recently set context will be returned.
     * If not set it from current class it will
     * be null.
     *
     * @return Context
     */
    public static final Context getContext() {
        return AppUtilities.context;
    }

    /**
     * First set context from every activity
     * before use any static method of AppUtils class.
     *
     * @param ctx
     */
    public static final void setContext(Context ctx) {
        AppUtilities.context = ctx;
    }

    /**
     * Get String from resource id
     *
     * @param res
     * @return
     */
    public static final String getStringFromResource(int res) {
        Context _ctx = getContext();
        if (null != _ctx) {
            try {
                return _ctx.getResources().getString(res);
            } catch (Resources.NotFoundException e) {
                Logger.printStackTrace(e);
                return "";
            }
        } else {
            Logger.showErrorLog("CONTEXT null");
            return "";
        }
    }

    /**
     * Check for email validation using android default
     * email validator.
     *
     * @param target
     * @return boolean
     */
    public static final boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivity(Class cls) {
        Context _ctx = getContext();
        if (null != _ctx) {
            Intent intent = new Intent(_ctx, cls);
            _ctx.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        } else {
            Logger.showErrorLog("CONTEXT null");
        }
    }


    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivityWithSingleExtra(Class cls, String stringExtra) {
        Context _ctx = getContext();
        if (null != _ctx) {
            Intent intent = new Intent(_ctx, cls);
            intent.putExtra("extra", stringExtra);
            _ctx.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        } else {
            Logger.showErrorLog("CONTEXT null");
        }
    }

    public static int getStatusBarHeight() {

        Rect rect = new Rect();
        Window win = ((Activity) getContext()).getWindow();
        win.getDecorView().

                getWindowVisibleDisplayFrame(rect);

        int statusBarHeight = rect.top;
        int contentViewTop = win.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;
        Log.d("ID-ANDROID-CONTENT", "################################statusBarHeight = " + statusBarHeight);
        if (statusBarHeight == 0) {
            statusBarHeight = 40;
        }
        return statusBarHeight;
    }


    public static void setSnackNotification(String message, View viewMain, int backgroundColor, int textColor,
                                            Boolean isActionBtnPresent, String actionText,
                                            int actionBtnTextColor) {
        if (viewMain != null) {

            final Snackbar snackbar = Snackbar
                    .make(viewMain, message, Snackbar.LENGTH_LONG);

            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(backgroundColor);

            TextView mySnackText = (TextView)
                    snackbarView.findViewById(android.support.design.R.id.snackbar_text);
            mySnackText.setTextColor(textColor);
            mySnackText.setGravity(Gravity.CENTER);

            if (isActionBtnPresent) {

                TextView mySnackActionText = (TextView)
                        snackbarView.findViewById(android.support.design.R.id.snackbar_action);
                mySnackActionText.setTextColor(actionBtnTextColor);
                mySnackActionText.setTypeface(Typeface.DEFAULT);
                snackbar.setAction(actionText, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });

            }


            snackbar.show();
        } else {
            Logger.showVerboseLog("snack bar View null");
        }
    }

    public static final void hideSoftInputMode(final EditText et) {
        Context _ctx = getContext();
        if (null != _ctx) {
            InputMethodManager im = (InputMethodManager) _ctx.getSystemService(_ctx.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(et.getWindowToken(), 0);
        } else {
            Logger.showErrorLog("CONTEXT null");
        }
    }

    public static void slide_down(Context ctx, View v) {
        Animation a = AnimationUtils.loadAnimation(ctx, R.anim.slide_down);
        if (a != null) {
            a.reset();
            if (v != null) {
                v.clearAnimation();
                v.startAnimation(a);
            }
        }
    }

    public static void slide_up(Context ctx, View v) {
        Animation a = AnimationUtils.loadAnimation(ctx, R.anim.slide_up);
        if (a != null) {
            a.reset();
            if (v != null) {
                v.clearAnimation();
                v.startAnimation(a);
            }
        }
    }

    // Loader with Loading text
    public static ProgressDialog PD(Context mContext, String message) {
        try {
            final ProgressDialog dialog = new ProgressDialog(mContext);
            dialog.setCanceledOnTouchOutside(false);
            try {
                dialog.show();
                ProgressBar pb = new ProgressBar(mContext);
                LinearLayout linearLayout = new LinearLayout(mContext);
                linearLayout.setGravity(Gravity.CENTER);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.addView(pb);
                TextView textView = new TextView(mContext);
                textView.setText(message);
                textView.setTextColor(Color.WHITE);
                textView.setGravity(Gravity.CENTER);
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                textView.setPadding(10, 10, 10, 10);
                linearLayout.addView(textView);
                dialog.setContentView(linearLayout);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            } catch (WindowManager.BadTokenException | IllegalStateException e) {
            }
            return dialog;

        } catch (NullPointerException e) {
            return null;
        }
    }

}
