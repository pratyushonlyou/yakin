package yakin.iserve.com.yakin.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.models.DeviceCompanyModelColorsModel;
import yakin.iserve.com.yakin.models.DeviceTierModel;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class DeviceTierArrayAdapter extends ArrayAdapter {


    Context c;
    ArrayList<DeviceTierModel> deviceTierModels;

    public DeviceTierArrayAdapter(@NonNull Context context, int resource, int textViewResourceId, ArrayList<DeviceTierModel> deviceTierModels) {
        super(context, resource, textViewResourceId, deviceTierModels);
        this.c = context;
        this.deviceTierModels = deviceTierModels;
    }

    @Override
    public int getCount() {
        return deviceTierModels.size();
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // return super.getDropDownView(position, convertView, parent);
        View v = LayoutInflater.from(c).inflate(R.layout.row_text_for_spinner, parent, false);

        TextView tv_row_text = (TextView) v.findViewById(R.id.tv_row_text);
        tv_row_text.setText(deviceTierModels.get(position).getTier_name()+"     "+deviceTierModels.get(position).getTier_range());

        return v;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(c).inflate(R.layout.row_text_for_spinner, parent, false);

        TextView tv_row_text = (TextView) v.findViewById(R.id.tv_row_text);
        tv_row_text.setText(deviceTierModels.get(position).getTier_name()+"     "+deviceTierModels.get(position).getTier_range());

        return v;
    }

    public void notifyAdapter(ArrayList<DeviceTierModel> deviceTierModels) {
        this.deviceTierModels = deviceTierModels;
        notifyDataSetChanged();
    }
}
