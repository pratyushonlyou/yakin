package yakin.iserve.com.yakin.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.models.DevicesModel;
import yakin.iserve.com.yakin.viewHolders.ExistingCustomerDevicesViewHolder;

/**
 * Created by AMIRULSUFI on 1/9/2018.
 */

public class ExistingCustomerDevicesRecyclerViewAdapter extends RecyclerView.Adapter<ExistingCustomerDevicesViewHolder> {


    Context c;
    ArrayList<DevicesModel> devicesModels;

    public ExistingCustomerDevicesRecyclerViewAdapter(Context c, ArrayList<DevicesModel> devicesModels) {
        this.c = c;
        this.devicesModels = devicesModels;
    }


    @Override
    public ExistingCustomerDevicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ExistingCustomerDevicesViewHolder(LayoutInflater.from(c).inflate(R.layout.row_existing_customer_devices, parent, false));
    }

    @Override
    public void onBindViewHolder(ExistingCustomerDevicesViewHolder holder, int position) {
        holder.tv_make.setText(devicesModels.get(position).getMake());
        holder.tv_model.setText(devicesModels.get(position).getModel());

        holder.tv_purchase_date.setText(devicesModels.get(position).getDevice_purchased_date());
        holder.tv_package.setText(devicesModels.get(position).getPackage_fee_id());
        holder.tv_validity.setText(devicesModels.get(position).getEnd_date());

    }

    @Override
    public int getItemCount() {
        return devicesModels.size();
    }
}
