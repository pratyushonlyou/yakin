package yakin.iserve.com.yakin.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.adapters.SignupRegionsArrayAdapter;
import yakin.iserve.com.yakin.constants.PermissionConstants;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.interfaces.APIParamConstants;
import yakin.iserve.com.yakin.interfaces.APIUrls;
import yakin.iserve.com.yakin.interfaces.APIJSONResponseConstants;
import yakin.iserve.com.yakin.models.RegionsModel;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener {


    private static final String TAG = "SignupActivity";

    EditText et_signup_username, et_signup_password, et_signup_con_password;//, et_signup_email;
    Button btn_signup;
    CheckBox chkbx_ref_code;
    EditText et_signup_ref_code;
    Spinner sp_region;
    EditText et_signup_retailer_name;

    ArrayList<RegionsModel> regions = new ArrayList<>();
    SignupRegionsArrayAdapter adapter;

    LinearLayout ll_container;

    String deviceId = "";

    TextInputLayout til_retailer_name, til_email, til_password, til_con_password, til_ref_code;
    String str_retailer_name = "", str_email = "", str_username = "", str_pass = "", str_con_pass = "", str_ref_code = "";
    String err_retailer_name = "", err_username = "", err_pass = "", err_con_pass = "", err_ref = "";
    String err_msg = "";
    int selected_region_id = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.sign_up));


        initViews();


    }


    private void initViews() {

        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        et_signup_username = (EditText) findViewById(R.id.et_signup_username);
        et_signup_password = (EditText) findViewById(R.id.et_signup_password);
        et_signup_con_password = (EditText) findViewById(R.id.et_signup_con_password);
//        et_signup_email = (EditText) findViewById(R.id.et_signup_email);
        btn_signup = (Button) findViewById(R.id.btn_signup);

        til_retailer_name = (TextInputLayout) findViewById(R.id.til_retailer_name);
        til_email = (TextInputLayout) findViewById(R.id.til_email);
        til_password = (TextInputLayout) findViewById(R.id.til_password);
        til_con_password = (TextInputLayout) findViewById(R.id.til_con_password);
        til_ref_code = (TextInputLayout) findViewById(R.id.til_ref_code);


        chkbx_ref_code = (CheckBox) findViewById(R.id.chkbx_ref_code);

        et_signup_ref_code = (EditText) findViewById(R.id.et_signup_ref_code);

        sp_region = (Spinner) findViewById(R.id.sp_region);
        et_signup_retailer_name = (EditText) findViewById(R.id.et_signup_retailer_name);


        setListeners();
//        setVals();
        regions.add(new RegionsModel("0", getResources().getString(R.string.select_region)));        // add a blank item it first position
        getRegions();

    }

    private void setVals() {

        adapter = new SignupRegionsArrayAdapter(SignupActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, regions);
        sp_region.setAdapter(adapter);
    }


    private void setListeners() {
        btn_signup.setOnClickListener(this);
        chkbx_ref_code.setOnCheckedChangeListener(this);
        sp_region.setOnItemSelectedListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                // sign up api call
                //finish();
                if (validate()) {
                    if (PermissionsCheck.checkPhoneStateAccessPermission(this)) {
                        getDeviceIMEI();
                    } else {
                        PermissionsCheck.requestPhoneStatePermission(SignupActivity.this);
                        return;
                    }
                }

                break;
        }

    }

    private boolean validate() {
        str_retailer_name = et_signup_retailer_name.getText().toString();
        str_username = et_signup_username.getText().toString();
        str_pass = et_signup_password.getText().toString();
        str_con_pass = et_signup_con_password.getText().toString();
        str_ref_code = et_signup_ref_code.getText().toString();

        if (str_retailer_name.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.retailer_name_empty), Snackbar.LENGTH_LONG).show();
            et_signup_retailer_name.requestFocus();
            return false;
        }
        if (str_username.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.username_empty), Snackbar.LENGTH_LONG).show();
            et_signup_username.requestFocus();
            return false;
        }
        /*if (!android.util.Patterns.EMAIL_ADDRESS.matcher(str_email).matches()) {
            Snackbar.make(rl_container, getResources().getString(R.string.email_invalid), Snackbar.LENGTH_LONG).show();
            et_signup_email.requestFocus();
            return false;
        }*/
        if (str_pass.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.password_empty), Snackbar.LENGTH_LONG).show();
            et_signup_password.requestFocus();
            return false;
        }
        if (str_pass.length() < 6 || str_pass.length() > 12) {
            Snackbar.make(ll_container, getResources().getString(R.string.password_length_err), Snackbar.LENGTH_LONG).show();
            et_signup_password.requestFocus();
            return false;
        }
        if (str_con_pass.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.con_password_empty), Snackbar.LENGTH_LONG).show();
            et_signup_con_password.requestFocus();
            return false;
        }
        if (!str_con_pass.equals(str_pass)) {
            Snackbar.make(ll_container, getResources().getString(R.string.pass_con_pass_mismatch), Snackbar.LENGTH_LONG).show();
            et_signup_con_password.requestFocus();
            return false;
        }
        if (str_ref_code.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.referral_code_empty), Snackbar.LENGTH_LONG).show();
            et_signup_ref_code.requestFocus();
            return false;
        }
        if (selected_region_id == 0) {
            Snackbar.make(ll_container, getResources().getString(R.string.region_empty), Snackbar.LENGTH_LONG).show();
            sp_region.performClick();
            return false;
        }

        return true;
    }


    public void getDeviceIMEI() {
        TelephonyManager telephonyManager;
        telephonyManager = (TelephonyManager) getSystemService(Context.
                TELEPHONY_SERVICE);
        /*
        * getDeviceId() returns the unique device ID.
        * For example,the IMEI for GSM and the MEID or ESN for CDMA phones.
        */
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        deviceId = telephonyManager.getDeviceId();
        Toast.makeText(SignupActivity.this, deviceId, Toast.LENGTH_LONG).show();


//        if (deviceId.equals("") || deviceId == null || Integer.parseInt(deviceId) == 0) {
//            getDeviceIMEI();
//            return;
//        }


        // clear the previous errors
        til_retailer_name.setError(null);
        til_email.setError(null);
        til_password.setError(null);
        til_con_password.setError(null);
        til_ref_code.setError(null);


        doRegistration();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            til_ref_code.setVisibility(View.VISIBLE);
        } else {
            til_ref_code.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionConstants.MY_PERMISSIONS_READ_PHONE_STATE) {
            if (PermissionsCheck.checkPhoneStateAccessPermission(this)) {
                getDeviceIMEI();
            } else {
                PermissionsCheck.requestPhoneStatePermission(SignupActivity.this);
                return;
            }

        }
    }


    private void getRegions() {
        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(ll_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqGetWithLoader(this, APIUrls.region_url, TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONArray jsonArr = new JSONArray(response);
                    if (jsonArr.length() > 0) {

                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject obj = jsonArr.getJSONObject(i);
                            RegionsModel model = new RegionsModel();
                            model.setId(obj.getString(APIJSONResponseConstants.id));
                            model.setCode(obj.getString(APIJSONResponseConstants.code));
                            model.setName(obj.getString(APIJSONResponseConstants.name));
                            model.setStatus_id(obj.getString(APIJSONResponseConstants.status_id));
                            model.setCountry_code(obj.getString(APIJSONResponseConstants.country_code));

                            regions.add(model);
                        }
                    }
                    setVals();

//                        Snackbar.make(rl_container,
//                                jsonObject.optString("message"), Snackbar.LENGTH_LONG).show();

                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Snackbar.make(ll_container,
                        getResources().getString(R.string.network_error), Snackbar.LENGTH_LONG).show();
            }
        });

    }


    private void doRegistration() {
        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(ll_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(APIParamConstants.name, str_retailer_name);
        hashMap.put(APIParamConstants.username, str_username);

        hashMap.put(APIParamConstants.password, str_pass);
        hashMap.put(APIParamConstants.password_confirmation, str_con_pass);
        hashMap.put(APIParamConstants.device_uid, deviceId);
        hashMap.put(APIParamConstants.role_id, APIParamConstants.retailer_role_id);    // 7 -> retailer, 9 -> customer
        hashMap.put(APIParamConstants.region, String.valueOf(selected_region_id));
//        hashMap.put(APIParamConstants.role_id, "");

        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqPostWithLoader(this, APIUrls.registration_url, TAG, hashMap, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {

                startActivity(new Intent(SignupActivity.this, DashBoardContainerActivity.class));
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                err_retailer_name = "";
                err_username = "";
                err_pass = "";
                err_con_pass = "";
                err_ref = "";
                err_msg = "";


                String json = null;


                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    err_retailer_name = HandleErrorMsgFromVolley.trimMessage(json, "name");
                    err_username = HandleErrorMsgFromVolley.trimMessage(json, "str_username");
                    err_pass = HandleErrorMsgFromVolley.trimMessage(json, "str_password");


                    show_errors();


                }

            }

        });
    }

    private void show_errors() {
        if (!err_msg.equals("")) {
            Snackbar.make(ll_container, err_msg, Snackbar.LENGTH_LONG).show();
        }
        if (!err_retailer_name.equals("")) {
            til_retailer_name.setError(err_retailer_name);
            et_signup_retailer_name.requestFocus();
        }
        if (!err_username.equals("")) {
            til_email.setError(err_username);
            et_signup_username.requestFocus();
        }
        if (!err_pass.equals("")) {
            til_password.setError(err_pass);
            et_signup_password.requestFocus();
        }
        if (!err_con_pass.equals("")) {
            til_con_password.setError(err_pass);
            et_signup_con_password.requestFocus();
        }

    }




    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_region:
                selected_region_id = Integer.parseInt(regions.get(position).getId());
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
