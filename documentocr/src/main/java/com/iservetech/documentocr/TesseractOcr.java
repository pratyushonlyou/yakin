package com.iservetech.documentocr;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.iservetech.documentocr.DocumentScannerActivity.TAG;

/**
 * Created by Ze Khan on 04-Jan-18.
 */

public class TesseractOcr {
    private static String datapath;
    private static String languageFile;
    private static TessBaseAPI mTess;

    public static void initialiseTesseract(boolean isPassport){
        new tesseractInitTask().execute(isPassport);
    }

    public static void processImage(Bitmap bitmap, boolean isPassport) {
        new ocrDetectionTask().execute(bitmap);
    }

    private static void checkFile(File dir) {
        if (!dir.exists() && dir.mkdirs()) {
            Log.d(TAG, "Directory does not exists");
            copyFiles();
        }
        //The directory exists, but there is no data file in it
        if (dir.exists()) {
            String datafilepath = datapath + "tessdata/" + languageFile + ".traineddata";
            File datafile = new File(datafilepath);
            if (!datafile.exists()) {
                Log.d(TAG, "Language file does not exist");
                copyFiles();
            }
        }
    }

    private static void copyFiles() {
        try {
            //location we want the file to be at
            String filepath = datapath + "tessdata/" + languageFile + ".traineddata";

            //get access to AssetManager
            AssetManager assetManager = DocumentScannerActivity.context.getAssets();

            //open byte streams for reading/writing
            InputStream instream = assetManager.open("tessdata/" + languageFile + ".traineddata");
            OutputStream outstream = new FileOutputStream(filepath);
            //copy the file to the location specified by filepath
            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {

                outstream.write(buffer, 0, read);
            }
            outstream.flush();
            outstream.close();
            instream.close();

        } catch (FileNotFoundException e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    private static class tesseractInitTask extends AsyncTask<Boolean,String,String>{
        @Override
        protected String doInBackground(Boolean... booleans) {
            datapath = DocumentScannerActivity.context.getFilesDir() + "/tesseract/";
            boolean isPassport = booleans[0];
            languageFile = isPassport ? "OCRB" : "eng";
            checkFile(new File(datapath + "tessdata/"));
            if (mTess == null) {
                mTess = new TessBaseAPI();
            }
            mTess.init(datapath, languageFile);

            return null;
        }
    }

    private static class ocrDetectionTask extends AsyncTask<Bitmap, Integer, String> {
        @Override
        protected String doInBackground(Bitmap... bitmaps) {

            mTess.setImage(bitmaps[0]);
            String ocrResult = mTess.getUTF8Text();
            Log.d(TAG, "ocr completed");
            Log.d(TAG, ocrResult);
            return null;
        }
    }
}
