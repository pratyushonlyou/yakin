package yakin.iserve.com.yakin.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import yakin.iserve.com.yakin.R;

/**
 * Created by AMIRULSUFI on 1/5/2018.
 */

public class TrainingPdfViewHolder extends RecyclerView.ViewHolder {

    public RelativeLayout rl_container;
    public ImageView iv_pdf_img;
    public TextView tv_posted_by, tv_posted_date, tv_desc;


    public TrainingPdfViewHolder(View itemView) {
        super(itemView);

        rl_container = (RelativeLayout) itemView.findViewById(R.id.rl_container);
        iv_pdf_img = (ImageView) itemView.findViewById(R.id.iv_pdf_img);
        tv_posted_by = (TextView) itemView.findViewById(R.id.tv_posted_by);
        tv_posted_date = (TextView) itemView.findViewById(R.id.tv_posted_date);
        tv_desc = (TextView) itemView.findViewById(R.id.tv_desc);

    }
}
