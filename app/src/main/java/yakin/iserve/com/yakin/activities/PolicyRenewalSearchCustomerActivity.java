package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.iservetech.documentocr.DocumentScannerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.OnActivityResultConstants;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.CheckValues;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.interfaces.APIParamConstants;
import yakin.iserve.com.yakin.interfaces.APIUrls;
import yakin.iserve.com.yakin.models.CustomerRegModel;
import yakin.iserve.com.yakin.models.DevicesModel;

import static yakin.iserve.com.yakin.constants.OnActivityResultConstants.IMEI_SCAN_REQUEST_CODE;
import static yakin.iserve.com.yakin.constants.OnActivityResultConstants.RC_OCR_CAPTURE;

public class PolicyRenewalSearchCustomerActivity extends AppCompatActivity implements View.OnClickListener {


    public static String TAG = "PolicyRenewalSearchCustomerActivity";
    RelativeLayout rl_container;
    TextView tv_read_myKad, tv_read_passport, tv_scan_imei;

    CustomerRegModel customerRegModel = new CustomerRegModel();

    String err_msg = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_policy_search_customer);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.policy_renewal));


        initViews();
        setListeners();
    }


    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.ll_container);
        tv_read_myKad = (TextView) findViewById(R.id.tv_read_myKad);
        tv_read_passport = (TextView) findViewById(R.id.tv_read_passport);
        tv_scan_imei = (TextView) findViewById(R.id.tv_scan_imei);


    }

    private void setListeners() {
        tv_read_myKad.setOnClickListener(this);
        tv_read_passport.setOnClickListener(this);
        tv_scan_imei.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_read_myKad:
                if (PermissionsCheck.checkCameraPermission(PolicyRenewalSearchCustomerActivity.this)) {
                    DocumentScannerActivity.readMyKad(PolicyRenewalSearchCustomerActivity.this, new DocumentScannerActivity.DocumentScannerListener() {
                        @Override
                        public void onOcrCompleted(String s) {
                            Log.d("tag", s);
                            String test = "";
//                            OpenCVLoader
                            try {
                                JSONObject obj = new JSONObject(s);
                                String address = "";
                                address = obj.optString("address").replace("\n", ",");

                                customerRegModel = new CustomerRegModel();
                                customerRegModel.setName(obj.optString("name"));
                                customerRegModel.setAddress(address);
                                customerRegModel.setIdentification_number(obj.optString("icNum"));
                                customerRegModel.setGender(obj.optString("gender"));


                                //api call
                                if (!customerRegModel.getIdentification_number().equals("")) {
                                    getExistingCustomerDetailsAPI();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                Snackbar.make(rl_container,getResources().getString(R.string.user_not_exist), Snackbar.LENGTH_LONG).show();
                            }

                        }
                    });
                } else {
                    PermissionsCheck.requestCameraPermission(PolicyRenewalSearchCustomerActivity.this);
                }
                break;
            case R.id.tv_read_passport:
                if (PermissionsCheck.checkCameraPermission(PolicyRenewalSearchCustomerActivity.this)) {
                    startActivityForResult(new Intent(PolicyRenewalSearchCustomerActivity.this, MyKadOcrCaptureActivity.class), RC_OCR_CAPTURE);
                } else {
                    PermissionsCheck.requestCameraPermission(PolicyRenewalSearchCustomerActivity.this);
                }
                break;
            case R.id.tv_scan_imei:
                if (PermissionsCheck.checkCameraPermission(PolicyRenewalSearchCustomerActivity.this)) {
                    startActivityForResult(new Intent(PolicyRenewalSearchCustomerActivity.this, SimpleScannerActivity.class), IMEI_SCAN_REQUEST_CODE);
                } else {
                    PermissionsCheck.requestCameraPermission(PolicyRenewalSearchCustomerActivity.this);
                }

                break;
        }
    }

    private void getExistingCustomerDetailsAPI() {
        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(rl_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqGetWithLoader(this, APIUrls.existing_customer_details + customerRegModel.getIdentification_number(), TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {


                try {
                    JSONObject obj = new JSONObject(response);

                    customerRegModel.setId(obj.optString("id"));
                    customerRegModel.setName(obj.optString("name"));
                    customerRegModel.setUsername(obj.optString("username"));
                    customerRegModel.setEmail(obj.optString("email"));
                    customerRegModel.setAddress(obj.optString("address"));
                    customerRegModel.setPhone_no(obj.optString("mobile_number"));
                    customerRegModel.setIdentification_type_id(obj.optString("identification_type_id"));
                    customerRegModel.setIdentification_number(obj.optString("identification_number"));
                    customerRegModel.setLanguage(String.valueOf(obj.optInt("language")));


                    JSONArray devices_arr = obj.optJSONArray("devices");
                    if(devices_arr.length()>0){
                        customerRegModel.devicesModels = new ArrayList<>();
                        for(int i=0; i<devices_arr.length();i++){
                            JSONObject device_obj = devices_arr.getJSONObject(i);
                            DevicesModel model = new DevicesModel();
                            model.setId(device_obj.optString("id"));
                            model.setSku_id(device_obj.optString("sku_id"));
                            model.setPackage_fee_id(device_obj.optString("package_fee_id"));
                            model.setImei(device_obj.optString("imei"));
                            model.setImei_2(device_obj.optString("imei_2"));
                            model.setMake(device_obj.optString("make"));
                            model.setModel(device_obj.optString("model"));
                            model.setOther_model(device_obj.optString("other_model"));

                            model.setDevice_purchased_date(device_obj.optString("device_purchased_date"));
                            model.setEnd_date(device_obj.optString("end_date"));

                            JSONObject sku = obj.optJSONObject("sku");
                            if (sku == null) {
                                model.setTier_id(device_obj.optString("tier_id"));
                            } else {
                                model.setTier_id(device_obj.optString("tier"));
                            }


                            customerRegModel.devicesModels.add(model);
                        }
                    }



                    Paper.book().write(PaperDBConstants.existing_customer_model, customerRegModel);
                    if (customerRegModel.getId().equals("") || customerRegModel.getId().equals(null)) {
                        Snackbar.make(rl_container, getResources().getString(R.string.user_not_exist), Snackbar.LENGTH_LONG).show();
                    }else {
                        Bundle b = new Bundle();
                        b.putParcelable("existingCustomerRegModel", customerRegModel);
                        Intent i = new Intent(PolicyRenewalSearchCustomerActivity.this, ExistingCustomerDetailsActivity.class);
                        i.putExtras(b);
                        startActivity(i);
//                    finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                err_msg = "";

                String json = null;
                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");

                }

            }

        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        startActivityForResult(new Intent(PolicyRenewalSearchCustomerActivity.this, SimpleScannerActivity.class), IMEI_SCAN_REQUEST_CODE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMEI_SCAN_REQUEST_CODE) {
//                String imei_code = data.getExtras().get(BarcodeObject).toString();
//                Snackbar.make(rl_container, imei_code, Snackbar.LENGTH_LONG).show();
                String imei_code = data.getExtras().get("Barcode").toString();


                if (CheckValues.checkIMEINo(PolicyRenewalSearchCustomerActivity.this, rl_container, imei_code)) {
                    Snackbar.make(rl_container, imei_code, Snackbar.LENGTH_LONG).show();

                    customerRegModel.setImei_no(imei_code);

                    Bundle b = new Bundle();
                    b.putParcelable("customerRegModel", customerRegModel);
                    Intent i = new Intent(PolicyRenewalSearchCustomerActivity.this, PolicyRenewalManualEntryActivity.class);
                    i.putExtras(b);
                    startActivity(i);
                }

            } else {

            }
        }
    }


}
