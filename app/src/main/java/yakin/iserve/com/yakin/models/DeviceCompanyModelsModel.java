package yakin.iserve.com.yakin.models;

import java.util.ArrayList;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class DeviceCompanyModelsModel {


    String model_id="", model_name="";
    public ArrayList<DeviceCompanyModelColorsModel> deviceCompanyModelColorsModels = new ArrayList<>();

    public DeviceCompanyModelsModel() {}


    public DeviceCompanyModelsModel(String id,String name) {
        this.model_id=id;
        this.model_name = name;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }
}
