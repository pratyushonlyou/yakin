package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.adapters.DeviceCompaniesArrayAdapter;
import yakin.iserve.com.yakin.adapters.DeviceCompanyModelColorsArrayAdapter;
import yakin.iserve.com.yakin.adapters.DeviceCompanyModelsArrayAdapter;
import yakin.iserve.com.yakin.adapters.DeviceTierArrayAdapter;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.dialogFragments.CustomCalendarDialogFragment;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.CheckValues;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.interfaces.APIParamConstants;
import yakin.iserve.com.yakin.interfaces.APIUrls;
import yakin.iserve.com.yakin.models.CustomerRegModel;
import yakin.iserve.com.yakin.models.DeviceCompaniesModel;
import yakin.iserve.com.yakin.models.DeviceCompanyModelColorsModel;
import yakin.iserve.com.yakin.models.DeviceCompanyModelsModel;
import yakin.iserve.com.yakin.models.DeviceTierModel;

import static yakin.iserve.com.yakin.constants.OnActivityResultConstants.IMEI_SCAN_REQUEST_CODE;
import static yakin.iserve.com.yakin.constants.OnActivityResultConstants.IMEI_SCAN_REQUEST_CODE_EXTRA;

public class CustomerDeviceDetailsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, CustomCalendarDialogFragment.OnDateSelect {


    public static String TAG = "CustomerDeviceDetailsActivity";
    LinearLayout ll_container;
    Spinner sp_device_company, sp_device_model, sp_device_color;
    EditText et_device_company_others, et_device_model_others, et_device_color_others;
    EditText et_imei_no;
    ImageView iv_scan_imei;
    TextView tv_purchase_date;
    Button btn_nxt;
    String date_str = "";

    ArrayList<DeviceCompaniesModel> deviceCompaniesModels = new ArrayList<>();

    DeviceCompaniesArrayAdapter companiesArrayAdapter;
    DeviceCompanyModelsArrayAdapter modelsArrayAdapter;
    DeviceCompanyModelColorsArrayAdapter colorsArrayAdapter;

    LinearLayout ll_imei_extra;
    EditText et_imei_no_extra;
    ImageView iv_scan_imei_extra;
    ImageView iv_scan_imei_add;
    ImageView iv_scan_imei_remove;

    CustomerRegModel customerRegModel = new CustomerRegModel();

    int selected_company_pos = 0, selected_model_pos = 0, selected_color_pos = 0;

    LinearLayout ll_tier;
    Spinner sp_device_tier;
    ArrayList<DeviceTierModel> deviceTierModels = new ArrayList<>();
    DeviceTierArrayAdapter deviceTierArrayAdapter;

    LinearLayout ll_color_sp_container;

    String sku_id = "", device_purchased_date = "", customer_id = "", imei = "", imei2 = "", model = "", make = "", other_model = "", tier = "";

    // error msg from backend
    String err_msg = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_device_details);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.manual_entry));


        if (getIntent().getExtras().getParcelable("customerRegModel") != null) {
            customerRegModel = getIntent().getExtras().getParcelable("customerRegModel");
        }

        initViews();

    }


    private void initViews() {
        ll_container = (LinearLayout) findViewById(R.id.ll_container);

        sp_device_company = (Spinner) findViewById(R.id.sp_device_company);
        sp_device_model = (Spinner) findViewById(R.id.sp_device_model);
        sp_device_color = (Spinner) findViewById(R.id.sp_device_color);

        et_device_company_others = (EditText) findViewById(R.id.et_device_company_others);
        et_device_model_others = (EditText) findViewById(R.id.et_device_model_others);
        et_device_color_others = (EditText) findViewById(R.id.et_device_color_others);
        et_imei_no = (EditText) findViewById(R.id.et_imei_no);
        iv_scan_imei = (ImageView) findViewById(R.id.iv_scan_imei);

        tv_purchase_date = (TextView) findViewById(R.id.tv_purchase_date);


        ll_imei_extra = (LinearLayout) findViewById(R.id.ll_imei_extra);
        et_imei_no_extra = (EditText) findViewById(R.id.et_imei_no_extra);
        iv_scan_imei_extra = (ImageView) findViewById(R.id.iv_scan_imei_extra);
        iv_scan_imei_add = (ImageView) findViewById(R.id.iv_scan_imei_add);
        iv_scan_imei_remove = (ImageView) findViewById(R.id.iv_scan_imei_remove);

        ll_color_sp_container = (LinearLayout) findViewById(R.id.ll_color_sp_container);
        ll_tier = (LinearLayout) findViewById(R.id.ll_tier);
        sp_device_tier = (Spinner) findViewById(R.id.sp_device_tier);

        btn_nxt = (Button) findViewById(R.id.btn_nxt);

//        generateDummyData();
        getDevicesAPI();
        setListeners();
        //setVals();

    }


    private void setVals() {
        companiesArrayAdapter = new DeviceCompaniesArrayAdapter(CustomerDeviceDetailsActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, deviceCompaniesModels);
        sp_device_company.setAdapter(companiesArrayAdapter);

        modelsArrayAdapter = new DeviceCompanyModelsArrayAdapter(CustomerDeviceDetailsActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, deviceCompaniesModels.get(0).deviceCompanyModelsModels);
        sp_device_model.setAdapter(modelsArrayAdapter);

        colorsArrayAdapter = new DeviceCompanyModelColorsArrayAdapter(CustomerDeviceDetailsActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, deviceCompaniesModels.get(0).deviceCompanyModelsModels.get(0).deviceCompanyModelColorsModels);
        sp_device_color.setAdapter(colorsArrayAdapter);


        deviceTierModels.add(new DeviceTierModel("1", "Tier 1", "RM 0 - RM 500"));
        deviceTierModels.add(new DeviceTierModel("2", "Tier 2", "RM 501 - RM 1000"));
        deviceTierModels.add(new DeviceTierModel("3", "Tier 3", "RM 1001 - RM 1500"));
        deviceTierModels.add(new DeviceTierModel("4", "Tier 4", "RM 1501 - RM 3000"));
        deviceTierModels.add(new DeviceTierModel("5", "Tier 5", "RM 3001 +"));

        deviceTierArrayAdapter = new DeviceTierArrayAdapter(CustomerDeviceDetailsActivity.this, R.layout.row_text_for_spinner, R.id.tv_row_text, deviceTierModels);
        sp_device_tier.setAdapter(deviceTierArrayAdapter);


    }

    private void setListeners() {

        sp_device_company.setOnItemSelectedListener(this);
        sp_device_model.setOnItemSelectedListener(this);
        sp_device_color.setOnItemSelectedListener(this);
        sp_device_tier.setOnItemSelectedListener(this);

        iv_scan_imei.setOnClickListener(this);
        tv_purchase_date.setOnClickListener(this);

        iv_scan_imei_extra.setOnClickListener(this);
        iv_scan_imei_add.setOnClickListener(this);
        iv_scan_imei_remove.setOnClickListener(this);

        btn_nxt.setOnClickListener(this);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_device_company:
                selected_company_pos = position;
//                if (deviceCompaniesModels.get(selected_company_pos).getCompany_id().equals("0")) {
//                    et_device_company_others.setText("");
//                    et_device_company_others.setVisibility(View.VISIBLE);
//                } else {
//                    et_device_company_others.setVisibility(View.GONE);
//                }

                // reinitializing to zero then necessary manipulation
                selected_model_pos = 0;
                selected_color_pos = 0;
                modelsArrayAdapter.notifyAdapter(deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels);
                colorsArrayAdapter.notifyAdapter(deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(0).deviceCompanyModelColorsModels);
                /*if (deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.size() == 1) {
                    et_device_model_others.setText("");
                    et_device_model_others.setVisibility(View.VISIBLE);
                } else {
                    et_device_model_others.setVisibility(View.GONE);
                }
                if (deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(0).deviceCompanyModelColorsModels.size() == 1) {
                    et_device_color_others.setText("");
                    et_device_color_others.setVisibility(View.VISIBLE);
                } else {
                    et_device_color_others.setVisibility(View.GONE);
                }*/
                sp_device_model.setSelection(0);
                sp_device_color.setSelection(0);


//                sp_device_model.setOnItemSelectedListener(null);
//                sp_device_model.setOnItemSelectedListener(CustomerDeviceDetailsActivity.this);
//                sp_device_model.setSelection(0);
//                sp_device_model.performClick();
                make = deviceCompaniesModels.get(selected_company_pos).getCompany_name();
                break;


            case R.id.sp_device_model:
                selected_model_pos = position;
//                if (deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).getModel_id().equals("0")) {
//                    et_device_model_others.setText("");
//                    et_device_model_others.setVisibility(View.VISIBLE);
//                } else {
//                    et_device_model_others.setVisibility(View.GONE);
//                }
                colorsArrayAdapter.notifyAdapter(deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).deviceCompanyModelColorsModels);

                selected_color_pos = 0;
//                if (deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).deviceCompanyModelColorsModels.size() == 1
//                        &&
//                        deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).deviceCompanyModelColorsModels.get(0).getColor_id().equals("0")) {
//                    et_device_color_others.setText("");
//                    et_device_color_others.setVisibility(View.VISIBLE);
//                } else {
//                    et_device_color_others.setVisibility(View.GONE);
//                }
                sp_device_color.setSelection(0);

                if (deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).getModel_id().equals("-1")) {
                    ll_color_sp_container.setVisibility(View.GONE);
                    et_device_model_others.setVisibility(View.VISIBLE);
                    //et_device_color_others.setVisibility(View.VISIBLE);
                    ll_tier.setVisibility(View.VISIBLE);

                    model = APIParamConstants.others;
                } else {
                    ll_color_sp_container.setVisibility(View.VISIBLE);
                    et_device_model_others.setVisibility(View.GONE);
                    et_device_color_others.setVisibility(View.GONE);
                    et_device_model_others.setText("");
                    et_device_color_others.setText("");
                    ll_tier.setVisibility(View.GONE);

                    model = deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).getModel_name();
                }


                break;

            case R.id.sp_device_color:
                selected_color_pos = position;
                if (deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).deviceCompanyModelColorsModels.get(position).getColor_id().equals("0")) {
                    et_device_color_others.setText("");
                    et_device_color_others.setVisibility(View.VISIBLE);
                    sku_id = "";
                } else {
                    et_device_color_others.setVisibility(View.GONE);
                    sku_id = deviceCompaniesModels.get(selected_company_pos).deviceCompanyModelsModels.get(selected_model_pos).deviceCompanyModelColorsModels.get(position).getColor_id();
                }


                break;

            case R.id.sp_device_tier:
                tier = deviceTierModels.get(position).getTier_id();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_nxt:
                if (validate()) {
                    addDeviceAPI();
                }
                break;
            case R.id.tv_purchase_date:
                CustomCalendarDialogFragment customCalendarDialogFragment = new CustomCalendarDialogFragment();
                customCalendarDialogFragment.callBack = this;
                customCalendarDialogFragment.show(getFragmentManager(), CustomCalendarDialogFragment.TAG);
                break;

            case R.id.iv_scan_imei:
//                startActivityForResult(new Intent(CustomerDeviceDetailsActivity.this, IMEIScanActivity.class), IMEI_SCAN_REQUEST_CODE);

                if (PermissionsCheck.checkCameraPermission(CustomerDeviceDetailsActivity.this)) {
//                    takePhoto();
                    startActivityForResult(new Intent(CustomerDeviceDetailsActivity.this, SimpleScannerActivity.class), IMEI_SCAN_REQUEST_CODE);
                } else {
                    PermissionsCheck.requestCameraPermission(CustomerDeviceDetailsActivity.this);
                }
                break;

            case R.id.iv_scan_imei_extra:
//                startActivityForResult(new Intent(CustomerDeviceDetailsActivity.this, IMEIScanActivity.class), IMEI_SCAN_REQUEST_CODE);

                if (PermissionsCheck.checkCameraPermission(CustomerDeviceDetailsActivity.this)) {
//                    takePhoto();
                    startActivityForResult(new Intent(CustomerDeviceDetailsActivity.this, SimpleScannerActivity.class), IMEI_SCAN_REQUEST_CODE_EXTRA);
                } else {
                    PermissionsCheck.requestCameraPermission(CustomerDeviceDetailsActivity.this);
                }
                break;
            case R.id.iv_scan_imei_add:
                ll_imei_extra.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_scan_imei_remove:
                ll_imei_extra.setVisibility(View.GONE);
                et_imei_no_extra.setText("");

                break;
        }
    }

    private boolean validate() {
        //sku_id  // manipulate in spinner onitemselectedlistener
        //device_purchased_date  // in onDateSet
        //="", device_purchased_date="", customer_id="", imei="", model="", make="", other_model="", tier_id="";
        customer_id = customerRegModel.getId();
        imei = et_imei_no.getText().toString().trim();
        imei2 = et_imei_no_extra.getText().toString().trim();
        //model : from listener
        // make : from listener
        other_model = et_device_model_others.getText().toString().trim();
        //tier_id : from listener

        if (!CheckValues.checkIMEINo(CustomerDeviceDetailsActivity.this, ll_container, et_imei_no.getText().toString())) {
            return false;
        }
        if (et_imei_no_extra.getText().toString().trim().length() > 0) {
            if (!CheckValues.checkIMEINo(CustomerDeviceDetailsActivity.this, ll_container, et_imei_no_extra.getText().toString())) {
                return false;
            }
        }
        if (device_purchased_date.equals("")) {
            Snackbar.make(ll_container, getResources().getString(R.string.purchese_date_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //startActivityForResult(new Intent(CustomerDeviceDetailsActivity.this, SimpleScannerActivity.class), IMEI_SCAN_REQUEST_CODE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onDateSelect(String type, String month_name, String dayOfWeek, int year, int month, int dayOfMonth, Calendar c_selected) {
        String y, m, d;
        if (dayOfMonth <= 9) {
            d = "0" + dayOfMonth;
        } else {
            d = dayOfMonth + "";
        }

        month++;
        if (month <= 9) {
            m = "0" + month;
        } else {
            m = month + "";
        }


        date_str = d + "-" + month_name + "-" + year;
        tv_purchase_date.setText(d + "-" + month_name + "-" + year);

        device_purchased_date = year + "/" + month + "/" + dayOfMonth;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMEI_SCAN_REQUEST_CODE) {
                String imei_code = data.getExtras().get("Barcode").toString();

                if (CheckValues.checkIMEINo(CustomerDeviceDetailsActivity.this, ll_container, imei_code)) {
                    et_imei_no.setText(imei_code);

                }
            } else if (requestCode == IMEI_SCAN_REQUEST_CODE_EXTRA) {
                String imei_code = data.getExtras().get("Barcode").toString();

                if (CheckValues.checkIMEINo(CustomerDeviceDetailsActivity.this, ll_container, imei_code)) {
                    et_imei_no_extra.setText(imei_code);
                }
            }
        }
    }


    private void getDevicesAPI() {

        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(ll_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqGetWithLoaderWithAccessTokenHeader(CustomerDeviceDetailsActivity.this, APIUrls.get_sku_url, TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {
                manipulateDeviceResponse(response);
                setVals();

            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                String json = null;

                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    String err_msg = "";
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    if (!err_msg.equals("")) {
                        Snackbar.make(ll_container, err_msg, Snackbar.LENGTH_LONG).show();
                    }
                }

            }


        });

    }

    private void manipulateDeviceResponse(String response) {
        try {
            JSONObject companies = new JSONObject(response);

            JSONArray companies_arr = companies.optJSONArray("companies");
            if (companies_arr.length() > 0) {
                for (int i = 0; i < companies_arr.length(); i++) {
                    JSONObject companyObj = companies_arr.optJSONObject(i);
                    DeviceCompaniesModel companiesModel = new DeviceCompaniesModel();
                    //companiesModel.setCompany_id(companyObj.getString("id"));
                    companiesModel.setCompany_name(companyObj.getString("name"));

                    JSONArray modelArr = companyObj.optJSONArray("models");
                    if (modelArr.length() > 0) {
                        for (int m = 0; m < modelArr.length(); m++) {
                            JSONObject modelObj = modelArr.optJSONObject(m);
                            DeviceCompanyModelsModel modelsModel = new DeviceCompanyModelsModel();
                            //modelsModel.setModel_id(modelObj.getString("id"));
                            modelsModel.setModel_name(modelObj.getString("name"));

                            JSONArray colorArr = modelObj.optJSONArray("colors");
                            if (colorArr.length() > 0) {
                                for (int c = 0; c < colorArr.length(); c++) {
                                    JSONObject colorObj = colorArr.optJSONObject(c);
                                    DeviceCompanyModelColorsModel colorsModel = new DeviceCompanyModelColorsModel();
                                    colorsModel.setColor_id(colorObj.optString("id"));
                                    colorsModel.setColor_name(colorObj.optString("name"));

                                    modelsModel.deviceCompanyModelColorsModels.add(colorsModel);
                                }

                            }

                            companiesModel.deviceCompanyModelsModels.add(modelsModel);
                        }
                        companiesModel.deviceCompanyModelsModels.add(new DeviceCompanyModelsModel("-1", "Other model"));
                    }
                    deviceCompaniesModels.add(companiesModel);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void addDeviceAPI() {
        if (!NetworkUtil.isConnected(this)) {
            Snackbar.make(ll_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }

        HashMap<String, String> hashMap = new HashMap<>();
        if (model.equals(APIParamConstants.others)) {
            sku_id = "";
        }
        hashMap.put(APIParamConstants.sku_id, sku_id);
        hashMap.put(APIParamConstants.device_purchased_date, device_purchased_date);
        hashMap.put(APIParamConstants.customer_id, customer_id);
        hashMap.put(APIParamConstants.imei, imei);
        hashMap.put(APIParamConstants.imei_2, imei2);
        hashMap.put(APIParamConstants.model, model);    // 7 -> retailer, 9 -> customer
        hashMap.put(APIParamConstants.make, make);
        hashMap.put(APIParamConstants.other_model, other_model);
        hashMap.put(APIParamConstants.tier_id, tier);


        NetworkRequest networkRequest = NetworkRequest.getInstance(this);
        networkRequest.strReqPostWithLoader(this, APIUrls.customer_add_device_url, TAG, hashMap, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {


                try {
                    JSONObject obj = new JSONObject(response);


                    JSONObject sku = obj.optJSONObject("sku");

                    if (sku == null) {
                        customerRegModel.setDevice_tier(obj.optString("tier_id"));
                    } else {
                        customerRegModel.setDevice_tier(sku.optString("tier"));
                    }

                    customerRegModel.setMake(make);
                    customerRegModel.setModel(model);
                    customerRegModel.setImei_no(imei);
                    customerRegModel.setImei_no2(imei2);
                    customerRegModel.setDevice_purchase_date(tv_purchase_date.getText().toString());

                    Paper.book().write(PaperDBConstants.customer_model, customerRegModel);
                    Paper.book().write(PaperDBConstants.customer_reg_last_accessed_page, PaperDBConstants.customer_reg_device_details_page);

                    startActivity(new Intent(CustomerDeviceDetailsActivity.this, CustomerRegistrationProductListingActivity.class));
//                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank

                err_msg = "";


                String json = null;


                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");

                    show_errors();


                }

            }

        });
    }

    private void show_errors() {
        if (!err_msg.equals("")) {
            Snackbar.make(ll_container, err_msg, Snackbar.LENGTH_LONG).show();
        }
    }
}
