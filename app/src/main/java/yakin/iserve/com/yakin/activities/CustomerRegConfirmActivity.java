package yakin.iserve.com.yakin.activities;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.models.CustomerRegModel;

public class CustomerRegConfirmActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_available_bal, tv_package_fee, tv_customer_name, tv_customer_address, tv_make, tv_model, tv_imei, tv_purchase_date;
    Button btn_confirm;

    CustomerRegModel customerRegModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_reg_confirm);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.app_name));

        customerRegModel = Paper.book().read(PaperDBConstants.customer_model);

        initViews();
        setVals();
        setListeners();
    }

    private void setListeners() {
        btn_confirm.setOnClickListener(this);
    }

    private void initViews() {
        tv_available_bal = (TextView) findViewById(R.id.tv_available_bal);
        tv_package_fee = (TextView) findViewById(R.id.tv_package_fee);
        tv_customer_name = (TextView) findViewById(R.id.tv_customer_name);
        tv_customer_address = (TextView) findViewById(R.id.tv_customer_address);
        tv_make = (TextView) findViewById(R.id.tv_make);
        tv_model = (TextView) findViewById(R.id.tv_model);
        tv_imei = (TextView) findViewById(R.id.tv_imei);
        tv_purchase_date = (TextView) findViewById(R.id.tv_purchase_date);
        btn_confirm = (Button) findViewById(R.id.btn_confirm);
    }

    private void setVals() {
//        tv_available_bal.setText();
        String fee = getResources().getString(R.string.currency) + " " + customerRegModel.getCustomerRegProductListModel().getPackage_fee();
        tv_package_fee.setText(fee);
        tv_customer_name.setText(customerRegModel.getName());
        tv_customer_address.setText(customerRegModel.getAddress());
        tv_make.setText(customerRegModel.getMake());
        tv_model.setText(customerRegModel.getModel());

        String imei = "";
        if (!customerRegModel.getImei_no2().equals("")) {
            imei = customerRegModel.getImei_no() + "\n" + customerRegModel.getImei_no2();
        }else{
            imei = customerRegModel.getImei_no();
        }
        tv_imei.setText(imei);
        tv_purchase_date.setText(customerRegModel.getDevice_purchase_date());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
//                startActivity(new Intent(CustomerRegConfirmActivity.this, PaymentGateWayActivity.class));

                Paper.book().write(PaperDBConstants.customer_reg_last_accessed_page, PaperDBConstants.customer_reg_confirm_page);

                startActivity(new Intent(CustomerRegConfirmActivity.this, CustomerSignatureActivity.class));

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
