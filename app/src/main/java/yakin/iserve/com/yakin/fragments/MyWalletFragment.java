package yakin.iserve.com.yakin.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.activities.PaymentGateWayActivity;
import yakin.iserve.com.yakin.adapters.MyWalletHistoryRecyclerViewAdapter;
import yakin.iserve.com.yakin.gatewayNInterfaces.MyNetworkResponse;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkRequest;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.HandleErrorMsgFromVolley;
import yakin.iserve.com.yakin.interfaces.APIUrls;
import yakin.iserve.com.yakin.models.MyWalletHistoryModel;

public class MyWalletFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    public static String TAG = "MyWalletFragment";

    View v;

    MyWalletHistoryRecyclerViewAdapter adapter;

    TextView tv_available_balance;
    RecyclerView rv_history;
    Button btn_reload;

    ArrayList<MyWalletHistoryModel> myWalletHistoryModels = new ArrayList<>();

    RelativeLayout rl_container;

    String wallet_balance="";


    public MyWalletFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyWalletFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyWalletFragment newInstance(String param1, String param2) {
        MyWalletFragment fragment = new MyWalletFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_my_wallet, container, false);


        initViews();
        getWalletAPI();
        getHistoryAPI();
        return v;


    }

    private void initViews() {
        rl_container = (RelativeLayout) v.findViewById(R.id.rl_container);
        tv_available_balance = (TextView) v.findViewById(R.id.tv_available_balance);
        rv_history = (RecyclerView) v.findViewById(R.id.rv_history);
        btn_reload = (Button) v.findViewById(R.id.btn_reload);

        setVals();
        setListeners();

    }

    private void setListeners() {
        btn_reload.setOnClickListener(this);
    }

    private void setVals() {

        myWalletHistoryModels = generateDummyData();
        rv_history.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MyWalletHistoryRecyclerViewAdapter(getActivity(), myWalletHistoryModels);
        rv_history.setAdapter(adapter);
    }

    private ArrayList<MyWalletHistoryModel> generateDummyData() {

        ArrayList<MyWalletHistoryModel> list = new ArrayList<>();
        MyWalletHistoryModel model1 = new MyWalletHistoryModel("1", "Mr. X", "28 Jan 2018", "15");
        MyWalletHistoryModel model2 = new MyWalletHistoryModel("2", "Mr. Y", "28 Jan 2018", "15");
        MyWalletHistoryModel model3 = new MyWalletHistoryModel("3", "Mr. S", "28 Jan 2018", "17");
        MyWalletHistoryModel model4 = new MyWalletHistoryModel("4", "Ms T", "26 Jan 2018", "15");
        MyWalletHistoryModel model5 = new MyWalletHistoryModel("5", "Mr. P", "26 Jan 2018", "15");
        MyWalletHistoryModel model6 = new MyWalletHistoryModel("6", "Mrs. D", "20 Jan 2018", "17");
        MyWalletHistoryModel model7 = new MyWalletHistoryModel("7", "Ms. L", "20 Jan 2018", "17");
        MyWalletHistoryModel model8 = new MyWalletHistoryModel("8", "Mr. A", "20 Jan 2018", "15");
        MyWalletHistoryModel model9 = new MyWalletHistoryModel("9", "Mr. B", "20 Jan 2018", "17");
        MyWalletHistoryModel model10 = new MyWalletHistoryModel("10", "Mr. C", "15 Jan 2018", "17");
        MyWalletHistoryModel model11 = new MyWalletHistoryModel("11", "Mr. D", "15 Jan 2018", "17");

        list.add(model1);
        list.add(model2);
        list.add(model3);
        list.add(model4);
        list.add(model5);
        list.add(model6);
        list.add(model7);
        list.add(model8);
        list.add(model9);
        list.add(model10);
        list.add(model11);

        return list;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_reload:
                startActivity(new Intent(getActivity(), PaymentGateWayActivity.class));
                break;
        }
    }





    private void getWalletAPI() {

        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(rl_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(getActivity());
        networkRequest.strReqGetWithLoaderWithAccessTokenHeader(getActivity(), APIUrls.wallet_info, TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {

                try {
                    JSONObject obj=new JSONObject(response);
                    wallet_balance=obj.optString("balance");
                    tv_available_balance.setText(getResources().getString(R.string.currency) + " " + wallet_balance);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                String json = null;

                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    String err_msg = "";
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    if (!err_msg.equals("")) {
                        Snackbar.make(rl_container, err_msg, Snackbar.LENGTH_LONG).show();
                    }
                }

            }
        });

    }

    private void getHistoryAPI() {

        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(rl_container,
                    getResources().getString(R.string.check_internet), Snackbar.LENGTH_LONG).show();
            return;
        }


        NetworkRequest networkRequest = NetworkRequest.getInstance(getActivity());
        networkRequest.strReqGetWithLoaderWithAccessTokenHeader(getActivity(), APIUrls.history, TAG, new MyNetworkResponse() {
            @Override
            public void onSuccessResponse(String response) {

//                try {
//                    JSONObject obj=new JSONObject(response);
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {

                // make all previous error msgs blank
                String json = null;

                NetworkResponse response = volleyError.networkResponse;
                if (response != null && response.data != null) {
//                    switch(response.statusCode){

                    json = new String(response.data);
                    String err_msg = "";
                    err_msg = HandleErrorMsgFromVolley.trimMessage(json, "message");
                    if (!err_msg.equals("")) {
                        Snackbar.make(rl_container, err_msg, Snackbar.LENGTH_LONG).show();
                    }
                }

            }
        });

    }

}
