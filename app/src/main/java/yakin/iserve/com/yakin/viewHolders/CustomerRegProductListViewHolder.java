package yakin.iserve.com.yakin.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import yakin.iserve.com.yakin.R;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class CustomerRegProductListViewHolder extends RecyclerView.ViewHolder {

    public RelativeLayout rl_customer_reg_product_row_container;
    public TextView tv_product,tv_product_desc;
    public RadioButton rbtn;

    public CustomerRegProductListViewHolder(View itemView) {
        super(itemView);

        rl_customer_reg_product_row_container = (RelativeLayout) itemView.findViewById(R.id.rl_customer_reg_product_row_container);
        tv_product = (TextView) itemView.findViewById(R.id.tv_product);
        tv_product_desc = (TextView) itemView.findViewById(R.id.tv_product_desc);
        rbtn = (RadioButton) itemView.findViewById(R.id.rbtn);
    }
}
