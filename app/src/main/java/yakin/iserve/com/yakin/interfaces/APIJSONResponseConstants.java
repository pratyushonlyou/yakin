package yakin.iserve.com.yakin.interfaces;

/**
 * Created by AMIRULSUFI on 12/28/2017.
 */

public interface APIJSONResponseConstants {

    String success = "success";

    // region
    String id="id";
    String name="name";
    String code = "code";
    String status_id="status_id";
    String country_code="country_code";
}
