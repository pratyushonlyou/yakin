package yakin.iserve.com.yakin.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.PaperDBConstants;

public class CustomerSignatureActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rl_container;

    CheckBox chkbx_t_n_c;
    Button btn_continue, btn_clear;
    TextView tv_t_n_c_link;

    File file;
    // Dialog dialog;
    LinearLayout ll_container;
    View view;
    //    signature mSignature;
    Bitmap bitmap;

    // Creating Separate Directory for saving Generated Images
    String DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/DigitSign/";
    String pic_name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    String StoredPath = DIRECTORY + pic_name + ".png";


    SignaturePad signature_pad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_signature);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.signature_page_title));


        initViews();


    }

    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);
        tv_t_n_c_link = (TextView) findViewById(R.id.tv_t_n_c_link);
        chkbx_t_n_c = (CheckBox) findViewById(R.id.chkbx_t_n_c);

        // Button to open signature panel
        btn_continue = (Button) findViewById(R.id.btn_continue);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_continue.setClickable(false);

//        rl_container = (LinearLayout) findViewById(R.id.rl_container);
//        mSignature = new signature(getApplicationContext(), null);
//        mSignature.setBackgroundColor(Color.WHITE);
        // Dynamically generating Layout through java code
//        ll_container.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        btn_continue.setEnabled(false);

//        view = ll_container;

//        btn_clear.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Log.v("tag", "Panel Cleared");
//                mSignature.clear();
//                btn_continue.setEnabled(false);
//
//                signature_pad.clear();
//            }
//        });
//        btn_continue.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//
//                Log.v("tag", "Panel Saved");
//                view.setDrawingCacheEnabled(true);
//                mSignature.save(view, StoredPath);
//                Toast.makeText(getApplicationContext(), "Successfully Saved", Toast.LENGTH_SHORT).show();
//                // Calling the same class
//                recreate();
//            }
//        });
//        // Method to create Directory, if the Directory doesn't exists
//        file = new File(DIRECTORY);
//        if (!file.exists()) {
//            file.mkdir();
//        }


        signature_pad = (SignaturePad) findViewById(R.id.signature_pad);


        setListeners();
    }

    private void setListeners() {
        tv_t_n_c_link.setOnClickListener(this);


        btn_clear.setOnClickListener(this);
        btn_continue.setOnClickListener(this);

        signature_pad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                btn_continue.setClickable(false);
            }

            @Override
            public void onSigned() {
                btn_continue.setClickable(true);
            }

            @Override
            public void onClear() {
                btn_continue.setClickable(false);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_clear:
                btn_continue.setClickable(false);
                signature_pad.clear();
                break;
            case R.id.btn_continue:
//                btn_continue.setClickable(false);
//                ImageView iv = (ImageView)findViewById(R.id.iv);
//                iv.setImageBitmap(loadBitmapFromView(signature_pad));
                if (chkbx_t_n_c.isChecked()) {
                    // api call
                    Paper.book().write(PaperDBConstants.customer_reg_last_accessed_page, PaperDBConstants.customer_reg_signature_page);
                    startActivity(new Intent(CustomerSignatureActivity.this, CustomerRegThankYouActivity.class));
                } else {
                    Snackbar.make(rl_container, getResources().getString(R.string.confirm_check_box_not_checked), Snackbar.LENGTH_LONG).show();
                }

                break;

            case R.id.tv_t_n_c_link:
//                if (!url.startsWith("http://") && !url.startsWith("https://"))
//                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(browserIntent);
                break;
        }
    }


    public static Bitmap loadBitmapFromView(View v) {//, int width, int height) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(0, 0, v.getLayoutParams().width, v.getLayoutParams().height);
        v.draw(c);
        return b;
    }


    /*public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v, String StoredPath) {
            Log.v("tag", "Width: " + v.getWidth());
            Log.v("tag", "Height: " + v.getHeight());
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(rl_container.getWidth(), rl_container.getHeight(), Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(bitmap);
            try {
                // Output the file
                FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
                v.draw(canvas);
                // Convert the output file to Image such as .png
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                mFileOutStream.flush();
                mFileOutStream.close();
            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            btn_continue.setEnabled(true);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
            Log.v("log_tag", string);
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
