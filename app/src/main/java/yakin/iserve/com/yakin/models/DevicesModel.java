package yakin.iserve.com.yakin.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AMIRULSUFI on 1/9/2018.
 */

public class DevicesModel implements Parcelable{

    String id = "";
    String sku_id="";
    String package_fee_id = "";
    String imei = "";
    String imei_2 = "";
    String make = "";
    String model = "";
    String other_model = "";
    String tier_id = "";
    String device_purchased_date = "";
    String end_date = "";


    public DevicesModel() {

    }

    protected DevicesModel(Parcel in) {
        id = in.readString();
        sku_id = in.readString();
        package_fee_id = in.readString();
        imei = in.readString();
        imei_2 = in.readString();
        make = in.readString();
        model = in.readString();
        other_model = in.readString();
        tier_id = in.readString();
        device_purchased_date = in.readString();
        end_date = in.readString();
    }

    public static final Creator<DevicesModel> CREATOR = new Creator<DevicesModel>() {
        @Override
        public DevicesModel createFromParcel(Parcel in) {
            return new DevicesModel(in);
        }

        @Override
        public DevicesModel[] newArray(int size) {
            return new DevicesModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public String getPackage_fee_id() {
        return package_fee_id;
    }

    public void setPackage_fee_id(String package_fee_id) {
        this.package_fee_id = package_fee_id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImei_2() {
        return imei_2;
    }

    public void setImei_2(String imei_2) {
        this.imei_2 = imei_2;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOther_model() {
        return other_model;
    }

    public void setOther_model(String other_model) {
        this.other_model = other_model;
    }

    public String getTier_id() {
        return tier_id;
    }

    public void setTier_id(String tier_id) {
        this.tier_id = tier_id;
    }

    public String getDevice_purchased_date() {
        return device_purchased_date;
    }

    public void setDevice_purchased_date(String device_purchased_date) {
        this.device_purchased_date = device_purchased_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(sku_id);
        dest.writeString(package_fee_id);
        dest.writeString(imei);
        dest.writeString(imei_2);
        dest.writeString(make);
        dest.writeString(model);
        dest.writeString(other_model);
        dest.writeString(tier_id);
        dest.writeString(device_purchased_date);
        dest.writeString(end_date);
    }
}
