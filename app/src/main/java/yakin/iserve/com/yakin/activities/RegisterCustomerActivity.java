package yakin.iserve.com.yakin.activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.iservetech.documentocr.DocumentScannerActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.constants.OnActivityResultConstants;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.models.CustomerRegModel;

import static yakin.iserve.com.yakin.constants.OnActivityResultConstants.TAKE_PICTURE;

public class RegisterCustomerActivity extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout rl_read_mykad, rl_read_passport, rl_manual_entry;

    private Uri imageUri;


    CustomerRegModel customerRegModel;// = new CustomerRegModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_customer);


        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);

        setTitle(getResources().getString(R.string.register_new_customer));

//        FaceDetector detector = new FaceDetector.Builder(this)
//                .setTrackingEnabled(false)
//                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
//                .build();


        initViews();

    }

    private void initViews() {
        rl_read_mykad = (RelativeLayout) findViewById(R.id.rl_read_mykad);
        rl_read_passport = (RelativeLayout) findViewById(R.id.rl_read_passport);
        rl_manual_entry = (RelativeLayout) findViewById(R.id.rl_manual_entry);


        setListeners();
    }

    private void setListeners() {
        rl_read_mykad.setOnClickListener(this);
        rl_read_passport.setOnClickListener(this);
        rl_manual_entry.setOnClickListener(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch ((v.getId())) {
            case R.id.rl_read_mykad:
                if (PermissionsCheck.checkCameraPermission(RegisterCustomerActivity.this)) {
//                    takePhoto();
                    //startActivityForResult(new Intent(RegisterCustomerActivity.this, MyKadOcrCaptureActivity.class), OnActivityResultConstants.RC_OCR_CAPTURE);
                    DocumentScannerActivity.readMyKad(RegisterCustomerActivity.this, new DocumentScannerActivity.DocumentScannerListener() {
                        @Override
                        public void onOcrCompleted(String s) {
                            Log.d("tag", s);
                            String test = "";
//                            OpenCVLoader
                            try {
                                JSONObject obj = new JSONObject(s);
                                String address = "";
                                address = obj.optString("address").replace("\n", ",");

                                customerRegModel = new CustomerRegModel();
                                customerRegModel.setName(obj.optString("name"));
                                customerRegModel.setAddress(address);
                                customerRegModel.setIdentification_number(obj.optString("icNum"));
                                customerRegModel.setGender(obj.optString("gender"));


                                Bundle b = new Bundle();
                                b.putParcelable("customerRegModel", customerRegModel);
                                Intent i = new Intent(RegisterCustomerActivity.this, ManualEntryActivity.class);
                                i.putExtras(b);
                                startActivity(i);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
//                    Intent i = new Intent(RegisterCustomerActivity.this, CustomerRegistrationProductListingActivity.class);
//
//                    startActivity(i);


                } else {
                    PermissionsCheck.requestCameraPermission(RegisterCustomerActivity.this);
                }
                break;


            case R.id.rl_read_passport:

                break;

            case R.id.rl_manual_entry:
                Bundle b = new Bundle();
                b.putParcelable("customerRegModel", customerRegModel);
                Intent i = new Intent(RegisterCustomerActivity.this, ManualEntryActivity.class);
                i.putExtras(b);
                startActivity(i);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        startActivityForResult(new Intent(RegisterCustomerActivity.this, MyKadOcrCaptureActivity.class),RC_OCR_CAPTURE);
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        imageUri = Uri.fromFile(photo);
        startActivityForResult(intent, TAKE_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = imageUri;
                    getContentResolver().notifyChange(selectedImage, null);
                    //ImageView imageView = (ImageView) findViewById(R.id.ImageView);
                    ContentResolver cr = getContentResolver();
                    Bitmap bitmap;
                    try {
                        bitmap = android.provider.MediaStore.Images.Media
                                .getBitmap(cr, selectedImage);

                        //imageView.setImageBitmap(bitmap);
                        Toast.makeText(this, selectedImage.toString(),
                                Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }

                break;
            case OnActivityResultConstants.RC_OCR_CAPTURE:
                if (resultCode == CommonStatusCodes.SUCCESS) {
                    if (data != null) {
                        customerRegModel = new CustomerRegModel();
                        String text = data.getStringExtra(MyKadOcrCaptureActivity.TextBlockObject);
                        customerRegModel.setName(text);
//                            Toast.makeText(RegisterCustomerActivity.this, text, Toast.LENGTH_LONG).show();
                        Log.d("TAGG", "Text read: " + text);
                    } else {

                        Log.d("TAGG", "No Text captured, intent data is null");
                    }
                } else {
//                        statusMessage.setText(String.format(getString(R.string.ocr_error),
//                                CommonStatusCodes.getStatusCodeString(resultCode)));
                }
                break;
        }
    }

}
