package yakin.iserve.com.yakin.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.adapters.TrainingMaterialPdfRecyclerViewAdapter;
import yakin.iserve.com.yakin.adapters.TrainingMaterialVideoRecyclerViewAdapter;
import yakin.iserve.com.yakin.dialogFragments.PdfViewerDialogFragment;
import yakin.iserve.com.yakin.dialogFragments.curl_effect.PdfCurlReaderDialogFragment;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.AppUtilities;
import yakin.iserve.com.yakin.helper.FIleDownloadUsingVolley;
import yakin.iserve.com.yakin.helper.Logger;
import yakin.iserve.com.yakin.helper.PermissionsCheck;
import yakin.iserve.com.yakin.models.TrainingPdfModel;
import yakin.iserve.com.yakin.models.TrainingVideoModel;

public class TrainingMaterialActivity extends AppCompatActivity implements TrainingMaterialPdfRecyclerViewAdapter.OnItemClick {


    LinearLayout ll_container;

    RecyclerView rv_videos, rv_pdf;
    TrainingMaterialVideoRecyclerViewAdapter videoAdapter;
    ArrayList<TrainingVideoModel> trainingVideoModels = new ArrayList<>();
    TrainingMaterialPdfRecyclerViewAdapter pdfAdapter;
    ArrayList<TrainingPdfModel> trainingPdfModels = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_material);

        ActionBar toolbar = getSupportActionBar();
        assert toolbar != null;
        toolbar.setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.training_material));


        initViews();
    }

    private void initViews() {
        ll_container = (LinearLayout) findViewById(R.id.ll_container);
        rv_videos = (RecyclerView) findViewById(R.id.rv_videos);
        rv_pdf = (RecyclerView) findViewById(R.id.rv_pdf);

        setVals();
    }

    private void setVals() {

        generateDummyData();

        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(TrainingMaterialActivity.this, OrientationHelper.HORIZONTAL, false);
        rv_videos.setLayoutManager(layoutManager1);

        videoAdapter = new TrainingMaterialVideoRecyclerViewAdapter(TrainingMaterialActivity.this, trainingVideoModels);
        rv_videos.setAdapter(videoAdapter);

        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(TrainingMaterialActivity.this, OrientationHelper.HORIZONTAL, false);
        rv_pdf.setLayoutManager(layoutManager2);

        pdfAdapter = new TrainingMaterialPdfRecyclerViewAdapter(TrainingMaterialActivity.this, trainingPdfModels);
        pdfAdapter.itemClickListener = this;
        rv_pdf.setAdapter(pdfAdapter);

    }

    private void generateDummyData() {
        trainingVideoModels.add(new TrainingVideoModel("1","http://techslides.com/demos/sample-videos/small.mp4","Mr. Admin1","1 jan 2018","Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum"));
        trainingVideoModels.add(new TrainingVideoModel("2","http://www.androidbegin.com/tutorial/AndroidCommercial.3gp","Mr. Admin1","1 jan 2018","Lorem Ipsum"));
        trainingVideoModels.add(new TrainingVideoModel("3","http://techslides.com/demos/sample-videos/small.mp4","Mr. Admin2","2 jan 2018","Lorem Ipsum"));

        trainingPdfModels.add(new TrainingPdfModel("http://www.pdf995.com/samples/pdf.pdf","https://lh3.googleusercontent.com/8VF8Oom0BIT89x24dFfhjexnQ2EQy_vNx5Qxob9rWxzPDyuc55IzB5POJ1Vcm2Xve4o2=w300","Admin 1","2 jan 2018","lorem ipsum"));
        trainingPdfModels.add(new TrainingPdfModel("http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf","https://lh3.googleusercontent.com/8VF8Oom0BIT89x24dFfhjexnQ2EQy_vNx5Qxob9rWxzPDyuc55IzB5POJ1Vcm2Xve4o2=w300","Admin 2","2 jan 2018","lorem ipsum"));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void setOnItemClickListener(int position) {
        if (PermissionsCheck.checkCameraPermission(TrainingMaterialActivity.this)) {

            afterPermissionGranted_onItemClick(position);
        }
    }


    private void afterPermissionGranted_onItemClick(int position) {
        TrainingPdfModel trainingPdfModel = trainingPdfModels.get(position);
        final String mUrl = trainingPdfModel.getPdf_link();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        final String name = mUrl.substring(mUrl.lastIndexOf('/') + 1);
        ContextWrapper cw = new ContextWrapper(TrainingMaterialActivity.this);
        File path = cw.getDir("yakin", Context.MODE_PRIVATE);
        if (!path.exists())
            path.mkdir();
        // File neFile
        final File mypath = new File(path,  name);
        if (mypath.exists()) {
            showPdfDialog(mypath, name);

        } else if (!NetworkUtil.isConnected(TrainingMaterialActivity.this)) {
            Snackbar.make(ll_container, getResources().getString(R.string.network_error) + ", " +
                    getResources().getString(R.string.pdf_not_downloaded), Snackbar.LENGTH_LONG).show();
            return;
        } else {
            final ProgressDialog PD = AppUtilities.PD(TrainingMaterialActivity.this,
                    AppUtilities.getStringFromResource(R.string.downloading));

            FIleDownloadUsingVolley request = new FIleDownloadUsingVolley(Request.Method.GET, mUrl,
                    new Response.Listener<byte[]>() {
                        @Override
                        public void onResponse(byte[] response) {
                            try {
                                if (response != null) {

                                    FileOutputStream out;
                                    Logger.showErrorLog("File Path" + mypath.getAbsolutePath() + "Size :" + response.length);
                                    mypath.createNewFile();
                                    out = new FileOutputStream(mypath);
                                    out.write(response, 0, response.length);
                                    out.flush();
                                    out.close();
                                    if (PD != null)
                                        PD.dismiss();
                                    showPdfDialog(mypath, name);

                                }
                            } catch (Exception e) {
                                if (PD != null)
                                    PD.dismiss();
                                Log.d("KEY_ERROR", "UNABLE TO DOWNLOAD FILE");
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (PD != null)
                        PD.dismiss();
                    error.printStackTrace();
                }
            }, null);
            RequestQueue mRequestQueue = Volley.newRequestQueue(TrainingMaterialActivity.this, new HurlStack());
            mRequestQueue.add(request);
        }

    }

    void showPdfDialog(File path, String bookName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            FragmentManager fm = getSupportFragmentManager();
            PdfCurlReaderDialogFragment alertDialog = PdfCurlReaderDialogFragment.
                    newInstance(path, bookName);
            alertDialog.show(fm, "fragment_curl");
        } else {
            FragmentManager fm = getSupportFragmentManager();
            PdfViewerDialogFragment alertDialog = PdfViewerDialogFragment.
                    newInstance(path, bookName);
            alertDialog.show(fm, "fragment_normal");
        }
    }
}
