package yakin.iserve.com.yakin.interfaces;

/**
 * Created by AMIRULSUFI on 12/28/2017.
 */

public interface APIUrls {

    String base_url = "http://192.168.0.205:8000/api/";

    String region_url = base_url+"region";
    String registration_url = base_url+"auth/register";
    String login_url = base_url+"auth/login";

    String user_url = base_url+"user";

    String get_sku_url = base_url+"sku";

    String customer_reg_url = base_url+"user/register";
    String customer_add_device_url = base_url+"user-device/add";
    String customer_package_fee_url = base_url+"package-fee";


    String existing_customer_details = base_url+"customer/";


    String wallet_info = base_url+"wallet";
    String history = base_url+"transaction/history";



}
