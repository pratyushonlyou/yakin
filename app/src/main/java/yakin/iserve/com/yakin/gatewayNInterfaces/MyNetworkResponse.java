package yakin.iserve.com.yakin.gatewayNInterfaces;

import com.android.volley.VolleyError;


public interface MyNetworkResponse {
    void onSuccessResponse(String response);
    void onErrorResponse(VolleyError volleyError);
}
