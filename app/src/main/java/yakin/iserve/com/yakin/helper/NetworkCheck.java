package yakin.iserve.com.yakin.helper;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by AMIRULSUFI on 12/8/2017.
 */

public class NetworkCheck {


    public static boolean isNetworkConnected(Context c) {
        ConnectivityManager cm = (ConnectivityManager)c. getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    public static boolean isInternetAvailable() {
//        try {
//            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
//            return !ipAddr.equals("");
//
//        } catch (Exception e) {
//            return false;
//        }
        return true;

    }
}
