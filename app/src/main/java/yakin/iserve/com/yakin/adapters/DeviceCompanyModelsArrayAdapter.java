package yakin.iserve.com.yakin.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.models.DeviceCompaniesModel;
import yakin.iserve.com.yakin.models.DeviceCompanyModelsModel;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class DeviceCompanyModelsArrayAdapter extends ArrayAdapter {


    Context c;
    ArrayList<DeviceCompanyModelsModel> deviceCompanyModelsModels;

    public DeviceCompanyModelsArrayAdapter(@NonNull Context context, int resource, int textViewResourceId, ArrayList<DeviceCompanyModelsModel> deviceCompanyModelsModels) {
        super(context, resource, textViewResourceId, deviceCompanyModelsModels);
        this.c = context;
        this.deviceCompanyModelsModels = deviceCompanyModelsModels;
    }

    @Override
    public int getCount() {
        return deviceCompanyModelsModels.size();
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
       // return super.getDropDownView(position, convertView, parent);
        View v = LayoutInflater.from(c).inflate(R.layout.row_text_for_spinner, parent, false);

        TextView tv_row_text = (TextView) v.findViewById(R.id.tv_row_text);
        tv_row_text.setText(deviceCompanyModelsModels.get(position).getModel_name());

        return v;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(c).inflate(R.layout.row_text_for_spinner, parent, false);

        TextView tv_row_text = (TextView) v.findViewById(R.id.tv_row_text);
        tv_row_text.setText(deviceCompanyModelsModels.get(position).getModel_name());

        return v;
    }


    public void notifyAdapter(ArrayList<DeviceCompanyModelsModel> modelLists){
        this.deviceCompanyModelsModels=modelLists;
        notifyDataSetChanged();
    }
}
