package yakin.iserve.com.yakin.dialogFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;

import java.io.File;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.helper.AppUtilities;

public class PdfViewerDialogFragment extends DialogFragment implements OnPageChangeListener,
        View.OnClickListener, TextView.OnEditorActionListener {

    private static PdfViewerDialogFragment pdfViewerDialogFragment;
    private static File mFile = null;
    private static String bookName = "";
    private Context mContext;
    private View rootView;
    private PDFView pdfView;
    private int pageNumber = 0, totalNumberOfPage = 1, width = 0;
    private Uri uri;
    private TextView dialogPdfViewer_tvName, dialogPdfViewer_etTotalPageNo;
    private EditText dialogPdfViewer_etPageNo;

    public PdfViewerDialogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.dialog_pdf_viewer, container, false);
        mContext = getActivity();
        Window mWindow = getDialog().getWindow();
        mWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return rootView;
    }

    private void initViews() {
        pdfView = (PDFView) rootView.findViewById(R.id.dialogPdfViewer_pdfView);
        dialogPdfViewer_tvName = (TextView) rootView.findViewById(R.id.dialogPdfViewer_tvName);
        rootView.findViewById(R.id.dialogPdfViewer_imvBack).setOnClickListener(this);
        rootView.findViewById(R.id.dialogPdfViewer_imvUp).setOnClickListener(this);
        rootView.findViewById(R.id.dialogPdfViewer_imvDown).setOnClickListener(this);
        dialogPdfViewer_etPageNo = (EditText) rootView.findViewById(R.id.dialogPdfViewer_etPageNo);
        dialogPdfViewer_etTotalPageNo = (TextView) rootView.findViewById(R.id.dialogPdfViewer_etTotalPageNo);
        dialogPdfViewer_etPageNo.setOnEditorActionListener(this);
        dialogPdfViewer_tvName.setText(bookName);
        if (mFile != null) {
            uri = Uri.fromFile(mFile);
            displayFromUri(uri);
        }


    }


    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume() {
        super.onResume();
        initViews();
    }

    public static PdfViewerDialogFragment newInstance(File file, String _bookName) {
        mFile = file;
        pdfViewerDialogFragment = new PdfViewerDialogFragment();
        bookName = _bookName;
        pdfViewerDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.dialogFragment);
        return pdfViewerDialogFragment;
    }

    private void displayFromUri(Uri uri) {

        pdfView.fromUri(uri)
                .defaultPage(pageNumber)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .load();
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        page = page + 1;
        dialogPdfViewer_etPageNo.setText("" + page);
        dialogPdfViewer_etPageNo.requestFocus(View.FOCUS_RIGHT);
        dialogPdfViewer_etTotalPageNo.setText(" / " + pageCount);
        pageNumber = page;
        totalNumberOfPage = pageCount;

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.dialogPdfViewer_imvBack:
                dismiss();
                break;
            case R.id.dialogPdfViewer_imvUp:
                if (pageNumber > 0 && totalNumberOfPage >= pageNumber) {
                    pageNumber = pageNumber - 1;
                    pdfView.jumpTo(pageNumber);
                }


                break;
            case R.id.dialogPdfViewer_imvDown:

                if (totalNumberOfPage >= pageNumber) {
                    pageNumber = pageNumber + 1;
                    pdfView.jumpTo(pageNumber);
                }
                break;
        }

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        int id = v.getId();
        switch (id) {
            case R.id.dialogPdfViewer_etPageNo:
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (dialogPdfViewer_etPageNo.getText().toString().isEmpty()
                            || Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()) == 0
                            || Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()) > totalNumberOfPage
                            || Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()) < 0) {

                        Snackbar.make((LinearLayout) rootView.findViewById(R.id.dialogPdfViewer_llParent),
                                AppUtilities.getStringFromResource(R.string.invalid_page_number), Snackbar.LENGTH_LONG).show();

                    } else {
                        pdfView.jumpTo(Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()));
                    }
                }
                break;
        }
        return false;
    }
}


