package yakin.iserve.com.yakin.activities;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;

import yakin.iserve.com.yakin.R;

public class CustomerImageSnapActivity extends AppCompatActivity implements View.OnClickListener {


    SurfaceView sv;
    Button btn_ok;

    private CameraSource cameraSource;
    private FaceDetector detector;


    private static final int REQUEST_PERMISSION = 20;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_customer_image_snap);

        sv = (SurfaceView) findViewById(R.id.sv);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(this);


        detector = new FaceDetector.Builder(this)
                .setProminentFaceOnly(false)
                .setTrackingEnabled(false)
                .setLandmarkType(FaceDetector.ALL_CLASSIFICATIONS)
                .build();


        detector.setProcessor(
                new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                        .build());


        buildCamera();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                takePic();
                break;
        }
    }


    private void takePic() {
        cameraSource.takePicture(null, new CameraSource.PictureCallback() {
            private File imageFile;

            @Override
            public void onPictureTaken(byte[] bytes) {
                try {
                    // convert byte array into bitmap
                    Bitmap loadedImage = null;
                    Bitmap rotatedBitmap = null;
                    loadedImage = BitmapFactory.decodeByteArray(bytes, 0,
                            bytes.length);

                    Matrix rotateMatrix = new Matrix();
//                    rotateMatrix.postRotate(rotation);
                    rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                            loadedImage.getWidth(), loadedImage.getHeight(),
                            rotateMatrix, false);

                    File dir = new File(
                            Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_PICTURES), "MyPhotos");

                    boolean success = true;
                    if (!dir.exists()) {
                        success = dir.mkdirs();
                    }
                    if (success) {
                        java.util.Date date = new java.util.Date();
                        imageFile = new File(dir.getAbsolutePath()
                                + File.separator
//                                + new Timestamp(date.getTime()).toString()
                                + "lastCustomerImg.jpg");

                        imageFile.createNewFile();
                    } else {
                        Toast.makeText(getBaseContext(), "Image Not saved",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ByteArrayOutputStream ostream = new ByteArrayOutputStream();

                    // save image into gallery
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);

                    FileOutputStream fout = new FileOutputStream(imageFile);
                    fout.write(ostream.toByteArray());
                    fout.close();
                    ContentValues values = new ContentValues();

                    values.put(MediaStore.Images.Media.DATE_TAKEN,
                            System.currentTimeMillis());
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                    values.put(MediaStore.MediaColumns.DATA,
                            imageFile.getAbsolutePath());

                    CustomerImageSnapActivity.this.getContentResolver().insert(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);



                    Intent i = new Intent();
                    i.putExtra("img_path", imageFile.getAbsolutePath());
                    setResult(RESULT_OK, i);
                    finish();
                    //saveToInternalStorage(loadedImage);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker();
        }
    }



    private class GraphicFaceTracker extends Tracker<Face> {


        @Override
        public void onNewItem(int faceId, Face face) {
            Log.i("Check:", "New Face");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_ok.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults,
                             final Face face) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_ok.setVisibility(View.VISIBLE);

                }
            });
        }

        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_ok.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onDone() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_ok.setVisibility(View.GONE);

                }
            });
        }
    }


    private void buildCamera() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        cameraSource = new CameraSource
                .Builder(this, detector)
//                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(true)
                .build();

        sv.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                ActivityCompat.requestPermissions(CustomerImageSnapActivity.this, new
                        String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA}, REQUEST_PERMISSION);
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startScan();
                } else {
                    Toast.makeText(CustomerImageSnapActivity.this, "Permission is Denied", Toast.LENGTH_LONG).show();
                }
        }
    }

    private void startScan() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(CustomerImageSnapActivity.this, "Permission is Denied", Toast.LENGTH_LONG).show();
                return;
            }
            cameraSource.start(sv.getHolder());
        } catch (IOException ie) {
            Log.e("CAMERA SOURCE", ie.getMessage());
        }
    }
}
