package yakin.iserve.com.yakin.dialogFragments.curl_effect;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.helper.AppUtilities;
import yakin.iserve.com.yakin.helper.Logger;
import yakin.iserve.com.yakin.interfaces.OnPageChange;


@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class PdfCurlReaderDialogFragment extends DialogFragment implements View.OnClickListener
        , TextView.OnEditorActionListener, OnPageChange {

    /**
     * Key string for saving the state of current page _index.
     */
    private static final String STATE_CURRENT_PAGE_INDEX = "current_page_index";
    private static PdfCurlReaderDialogFragment pdfViewerDialogFragment;
    private static String bookName = "";
    private static File mFile = null;

    /**
     * File descriptor of the PDF.
     */
    private ParcelFileDescriptor mFileDescriptor;

    /**
     * {@link PdfRenderer} to render the PDF.
     */
    private PdfRenderer mPdfRenderer;

    /**
     * Page that is currently shown on the screen.
     */
    private PdfRenderer.Page mCurrentPage;


    private CurlView mCurlView;
    ArrayList<Bitmap> bitmaps = new ArrayList<>();
    View rootView;
    private TextView dialogPdfViewer_tvName, dialogPdfViewer_etTotalPageNo;
    private EditText dialogPdfViewer_etPageNo;
    private int totalNumberOfPage = 0, _index = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_curl_view, container, false);
        initViews();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (null != savedInstanceState) {
            _index = savedInstanceState.getInt(STATE_CURRENT_PAGE_INDEX, 1);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(STATE_CURRENT_PAGE_INDEX, _index);

    }

    private void initViews() {

        mCurlView = (CurlView) rootView.findViewById(R.id.curl);
        rootView.findViewById(R.id.dialogPdfViewer_imvBack).setOnClickListener(this);
        dialogPdfViewer_tvName = (TextView) rootView.findViewById(R.id.dialogPdfViewer_tvName);
        dialogPdfViewer_etPageNo = (EditText) rootView.findViewById(R.id.dialogPdfViewer_etPageNo);
        dialogPdfViewer_etTotalPageNo = (TextView) rootView.findViewById(R.id.dialogPdfViewer_etTotalPageNo);
        dialogPdfViewer_tvName.setText(bookName);
        dialogPdfViewer_etPageNo.setOnEditorActionListener(this);
        mCurlView.setOnPageChangeListener(this);

        try {
            openRenderer(getActivity(), mFile);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();

        }

    }


    @Override
    public void onPause() {
        super.onPause();
        mCurlView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCurlView.onResume();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.dialogPdfViewer_imvBack:
                dismiss();
                break;

        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        int id = v.getId();
        switch (id) {
            case R.id.dialogPdfViewer_etPageNo:
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (dialogPdfViewer_etPageNo.getText().toString().isEmpty()
                            || Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()) == 0
                            || Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()) > totalNumberOfPage
                            || Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()) < 0) {

                        Snackbar.make((RelativeLayout) rootView.findViewById(R.id.dialogPdfViewer_rlParent),
                                AppUtilities.getStringFromResource(R.string.invalid_page_number), Snackbar.LENGTH_LONG).show();

                    } else {
                        _index = Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString());
                        mCurlView.setCurrentIndex(Integer.parseInt(dialogPdfViewer_etPageNo.getText().toString()) - 1);
                    }
                }
                break;
        }
        return false;
    }

    @Override
    public void setOnPageChangeListenerLeft(final int pos) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (_index > 0) {
                    _index = _index - 1;
                    dialogPdfViewer_etPageNo.setText("" + _index);
                }
            }
        });


    }

    @Override
    public void setOnPageChangeListenerRight(final int pos) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (_index < totalNumberOfPage) {
                    _index = _index + 1;
                    dialogPdfViewer_etPageNo.setText("" + _index);
                }
            }
        });


    }


    /**
     * Bitmap provider.
     */
    private class PageProvider implements CurlView.PageProvider {
        PdfRenderer pdfRenderer;
        int COUNT;
        OnPageChange onPageChange;


        public PageProvider(PdfRenderer pdfRenderer, OnPageChange onPageChange) {
            this.pdfRenderer = pdfRenderer;
            COUNT = pdfRenderer.getPageCount();
            this.onPageChange = onPageChange;
        }

        @Override
        public int getPageCount() {
            return COUNT;
        }

        private Bitmap loadBitmap(int width, int height, int index) {

            if (null != mCurrentPage) {
                mCurrentPage.close();
            }
            mCurrentPage = mPdfRenderer.openPage(index);
            // Important: the destination bitmap must be ARGB (not RGB).
            Bitmap bitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);
            mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_PRINT);
            return bitmap;
        }

        @Override
        public void updatePage(CurlPage page, int width, int height, int index) {
            if (index == 0) {
                Logger.showErrorLog("#######_index == 0 :" + index);
                Bitmap front = loadBitmap(width, height, index);
                Bitmap back = loadBitmap(width, height, index);
                page.setTexture(front, CurlPage.SIDE_FRONT);
                page.setTexture(back, CurlPage.SIDE_BACK);
                return;
            } else if (index == (COUNT - 1)) {
                Logger.showErrorLog("#######_index == (COUNT - 1) == 0 :" + index);
                Bitmap front = loadBitmap(width, height, index);
                page.setTexture(front, CurlPage.SIDE_BOTH);
                return;
            } else {
                Logger.showErrorLog("#######Else == Other :" + index);
                Bitmap front = loadBitmap(width, height, index);
                Bitmap back = loadBitmap(width, height, index);
                page.setTexture(front, CurlPage.SIDE_FRONT);
                page.setTexture(back, CurlPage.SIDE_BACK);
            }

        }

    }


    /**
     * CurlView size changed observer.
     */
    private class SizeChangedObserver implements CurlView.SizeChangedObserver {
        @Override
        public void onSizeChanged(int w, int h) {
            if (w > h) {
                mCurlView.setViewMode(CurlView.SHOW_TWO_PAGES);
                mCurlView.setMargins(.02f, .0f, .0f, .0f);
            } else {
                mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
                mCurlView.setMargins(.02f, .0f, .0f, .0f);
            }
        }
    }

    /**
     * Sets up a {@link PdfRenderer} and related resources.
     */
    private void openRenderer(Context context, File mFile) throws IOException {
        File fileOne = new File(Environment.getExternalStorageDirectory() + "/pdf.pdf");

        try {
            mFileDescriptor = ParcelFileDescriptor.open(mFile, ParcelFileDescriptor.MODE_READ_ONLY);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        mPdfRenderer = new PdfRenderer(mFileDescriptor);
        mCurlView.setSizeChangedObserver(new SizeChangedObserver());
        mCurlView.setPageProvider(new PageProvider(mPdfRenderer, this));
        mCurlView.setCurrentIndex(0);
        mCurlView.setBackgroundColor(0xFF202830);
        totalNumberOfPage = mPdfRenderer.getPageCount();
        dialogPdfViewer_etTotalPageNo.setText(" / " + totalNumberOfPage);
    }

    public static PdfCurlReaderDialogFragment newInstance(File file, String _bookName) {
        mFile = file;
        pdfViewerDialogFragment = new PdfCurlReaderDialogFragment();
        bookName = _bookName;
        pdfViewerDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.dialogFragment);
        return pdfViewerDialogFragment;
    }

}