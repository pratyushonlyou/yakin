package yakin.iserve.com.yakin.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import io.paperdb.Paper;
import yakin.iserve.com.yakin.R;
import yakin.iserve.com.yakin.activities.VideoViewActivity;
import yakin.iserve.com.yakin.constants.PaperDBConstants;
import yakin.iserve.com.yakin.gatewayNInterfaces.NetworkUtil;
import yakin.iserve.com.yakin.helper.AppUtilities;
import yakin.iserve.com.yakin.helper.FIleDownloadUsingVolley;
import yakin.iserve.com.yakin.helper.Logger;
import yakin.iserve.com.yakin.models.TrainingPdfModel;
import yakin.iserve.com.yakin.viewHolders.TrainingPdfViewHolder;

/**
 * Created by AMIRULSUFI on 1/4/2018.
 */

public class TrainingMaterialPdfRecyclerViewAdapter extends RecyclerView.Adapter<TrainingPdfViewHolder>  {

    public static String TAG = "TrainingMaterialVideoRecyclerViewAdapter";
    Context c;
    ArrayList<TrainingPdfModel> trainingPdfModels;
    public OnItemClick itemClickListener;


    public TrainingMaterialPdfRecyclerViewAdapter(Context c, ArrayList<TrainingPdfModel> trainingPdfModels) {
        this.c = c;
        this.trainingPdfModels = trainingPdfModels;
    }


    @Override
    public TrainingPdfViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TrainingPdfViewHolder(LayoutInflater.from(c).inflate(R.layout.row_training_pdf, parent, false));
    }

    @Override
    public void onBindViewHolder(final TrainingPdfViewHolder holder, final int position) {

        Glide.with(c).load(trainingPdfModels.get(position).getPdf_img())
                .thumbnail(0.5f)
                .crossFade()
                .placeholder(c.getResources().getDrawable(R.drawable.ic_pdf))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_pdf_img);

        holder.tv_posted_by.setText(trainingPdfModels.get(position).getPdf_uolpaded_by());
        holder.tv_posted_date.setText(trainingPdfModels.get(position).getPdf_uploaded_date());
        holder.tv_desc.setText(trainingPdfModels.get(position).getDesc());



        holder.rl_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemClickListener.setOnItemClickListener(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return trainingPdfModels.size();
    }










    public interface OnItemClick {
        void setOnItemClickListener(int position);

    }
}
