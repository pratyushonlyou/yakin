package yakin.iserve.com.yakin.models;

import java.util.ArrayList;

/**
 * Created by AMIRULSUFI on 12/26/2017.
 */

public class DeviceCompaniesModel {


    String company_id, company_name;
    public ArrayList <DeviceCompanyModelsModel> deviceCompanyModelsModels = new ArrayList<>();


    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }
}
